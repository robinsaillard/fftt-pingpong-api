<?php


namespace FFTTPingpongApi\Model\Poule;

use FFTTPingpongApi\Model\Rencontre\Rencontre;

class PouleResult extends Rencontre
{
    /**
     * @var string $numeroClubA
     */
    private $numeroClubA;

    /**
     * @var string $numeroClubB
     */
    private $numeroClubB;

    /**
     * @var string $pouleId
     */
    private $pouleId;

    /**
     * @var bool $isLive
     */
    private $isLive;
    
    /**
     * Poule constructor
     * 
     * @param string $url A string containing the poule ID and the division ID.
     * @param string $libelle The parameter "libelle" is a string that represents the label or name of
     * the poule.
     */
    public function __construct(
        string $libelle,
        string $nomEquipeA,
        string $nomEquipeB,
        int $scoreEquipeA,
        int $scoreEquipeB,
        string $lien,
        \DateTime $datePrevue,
        ?\DateTime $dateReelle,
        string $numeroClubA,
        string $numeroClubB,
        string $pouleId,
        bool $isLive,
    )
    {
        parent::__construct($libelle, $nomEquipeA, $nomEquipeB, $scoreEquipeA, $scoreEquipeB, $lien, $datePrevue, $dateReelle);
        $this->numeroClubA = $numeroClubA;
        $this->numeroClubB = $numeroClubB;
        $this->pouleId = $pouleId;
        $this->isLive = $isLive;
    }

    /**
     * @return string
     */
    public function getNumeroClubA(): string
    {
        return $this->numeroClubA;
    }

    /**
     * @return string
     */
    public function getNumeroClubB(): string
    {
        return $this->numeroClubB;
    }

    /**
     * @return string
     */
    public function getPouleId(): string
    {
        return $this->pouleId;
    }

    /**
     * @return bool
     */
    public function IsLive(): bool
    {
        return $this->isLive;
    }
}