<?php

namespace FFTTPingpongApi\Model\Enums;

class AbstractEnum
{
    /**
     * Check if type exists is enum
     *
     * @param array $cases
     * @param string $searchValue
     * @return bool
     */
    public static function isExisting(array $cases, string $searchValue): bool
    {
        foreach ($cases as $case) {
            if ($searchValue === $case->value)
                return true;
        }
        return false;
    }
}