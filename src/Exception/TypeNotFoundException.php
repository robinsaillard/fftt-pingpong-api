<?php


namespace FFTTPingpongApi\Exception;


class TypeNotFoundException extends \Exception
{
    public function __construct($type)
    {
        parent::__construct(
            sprintf(
                "Le type '%s' n'existe pas.",
                $type
            )
        );
    }
}