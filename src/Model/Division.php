<?php


namespace FFTTPingpongApi\Model;

class Division
{
    /**
     * @var string $divisionId
     */
    private $divisionId;

    /**
     * @var string $libelle
     */
    private $libelle;
    
    /**
     * Division constructor
     * 
     * @param string $divisionId A string representing the division ID.
     * @param string $libelle The parameter "libelle" is a string that represents the label or name of
     * the division.
     */
    public function __construct(string $divisionId, string $libelle)
    {
        $this->divisionId = $divisionId;
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getDivisionId(): string
    {
        return $this->divisionId;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }
}