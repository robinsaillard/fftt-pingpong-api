<?php


namespace FFTTPingpongApi\Model\Rencontre;


class Partie
{
    /**
     * @var string $adversaireA
     */
    private $adversaireA;

    /**
     * @var string $adversaireB
     */
    private $adversaireB;

    /**
     * @var int $scoreA
     */
    private $scoreA;

    /**
     * @var int $scoreB
     */
    private $scoreB;

    /**
     * @var string[] $setDetails
     */
    private $setDetails;

    /**
     * Partie constructor
     * 
     * @param string $adversaireA The name of the first adversary or player in the game or match.
     * @param string $adversaireB The parameter "adversaireB" is a string that represents the name of
     * the second adversary in a game or match.
     * @param int $scoreA The parameter `scoreA` represents the score of `adversaireA` in a game or
     * match.
     * @param int $scoreB The parameter `scoreB` represents the score of `adversaireB` in a game or
     * match.
     * @param array $setDetails The "setDetails" parameter is an array that contains the details of each
     * set played in the match. Each element of the array represents a set and contains information
     * such as the score of each player in that set.
     */
    public function __construct(string $adversaireA, string $adversaireB, int $scoreA, int $scoreB, array $setDetails)
    {
        $this->adversaireA = $adversaireA;
        $this->adversaireB = $adversaireB;
        $this->scoreA = $scoreA;
        $this->scoreB = $scoreB;
        $this->setDetails = $setDetails;
    }

    /**
     * @return string
     */
    public function getAdversaireA(): string
    {
        return $this->adversaireA;
    }

    /**
     * @return string
     */
    public function getAdversaireB(): string
    {
        return $this->adversaireB;
    }

    /**
     * @return int
     */
    public function getScoreA(): int
    {
        return $this->scoreA;
    }

    /**
     * @return int
     */
    public function getScoreB(): int
    {
        return $this->scoreB;
    }

    /**
     * @return string[]
     */
    public function getSetDetails(): array
    {
        return $this->setDetails;
    }
}