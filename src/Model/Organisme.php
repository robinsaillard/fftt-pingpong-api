<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 27/11/18
 * Time: 19:24
 */

namespace FFTTPingpongApi\Model;


class Organisme
{
    /**
     * @var string $libelle
     */
    private $libelle;

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $code
     */
    private $code;

    /**
     * @var int|null $idpere
     */
    private $idpere;

    /**
     * Organisme constructor
     * 
     * @param string $libelle The "libelle" parameter is a string that represents the label or name of
     * the organism.
     * @param int $id The id parameter is an integer that represents the unique identifier of the
     * Organisme object.
     * @param string $code The "code" parameter is a string that represents the code of the
     * organization. It is used to uniquely identify the organization.
     * @param int $idpere The parameter "idpere" represents the ID of the parent organization. It is
     * used to establish a hierarchical relationship between organizations.
     */
    public function __construct(string $libelle, int $id, string $code, int|null $idpere)
    {
        $this->libelle = $libelle;
        $this->id = $id;
        $this->code = $code;
        $this->idpere = $idpere;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return int|null
     */
    public function getIdpere(): ?int
    {
        return $this->idpere;
    }
}