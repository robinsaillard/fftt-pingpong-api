<?php


namespace FFTTPingpongApi\Service;

use FFTTPingpongApi\FFTTApi;
use FFTTPingpongApi\Model\Club;
use FFTTPingpongApi\Model\Equipe;
use FFTTPingpongApi\Model\Joueur;
use FFTTPingpongApi\Model\Partie;
use FFTTPingpongApi\Model\Epreuve;
use FFTTPingpongApi\Model\Division;
use FFTTPingpongApi\Model\Actualite;
use FFTTPingpongApi\Model\Organisme;
use FFTTPingpongApi\Model\Classement;
use FFTTPingpongApi\Model\Historique;
use FFTTPingpongApi\Model\ClubDetails;
use FFTTPingpongApi\Model\EquipePoule;
use FFTTPingpongApi\Model\Poule\Poule;
use FFTTPingpongApi\Model\JoueurDetails;
use FFTTPingpongApi\Model\Poule\PouleResult;
use FFTTPingpongApi\Model\UnvalidatedPartie;
use FFTTPingpongApi\Model\Rencontre\Rencontre;
use FFTTPingpongApi\Model\Poule\PouleClassement;
use FFTTPingpongApi\Model\ResultIndividuel\Tour;
use FFTTPingpongApi\Model\Rencontre\RencontreDetails;
use FFTTPingpongApi\Model\ResultIndividuel\PartieIndividuel;
use FFTTPingpongApi\Model\ResultIndividuel\ClassementIndividuel;

class EntityFactory
{
    /**
     * @var FFTTApi|null $api
     */
    private $api;

    /**
     * EntityFactory constructor
     * 
     * @param FFTTApi|null $api The "api" parameter is an instance of the FFTTApi class. It is being injected
     * into the constructor of the current class.
     */
    public function __construct(?FFTTApi $api = null)
    {
        $this->api = $api;
    }

    public function __call($name, $arguments)
    {
        $reflection = new \ReflectionMethod(self::class, $name);
        if (empty($arguments[0]))
            return [];
        if ($reflection->getReturnType() == 'array')
            $arguments[0] = Utils::wrappedArrayIfUnique($arguments[0]);
        return call_user_func_array(array($this, $name), $arguments);
    }

    /**
     * @param array $data
     * @return Actualite[]
     */
    private function actualiteFactory(array $data) : array {
        $result = [];
        foreach ($data as $dataActualite) {
            $result[] = new Actualite(
                \DateTime::createFromFormat('Y-m-d', $dataActualite["date"]),
                $dataActualite['titre'],
                $dataActualite['description'],
                $dataActualite['url'],
                $dataActualite['photo'],
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Classement
     */
    private function classementFactory(array $data) : Classement {
        return new Classement(
            $data['licence'],
            $data['nom'],
            $data['prenom'],
            $data['club'],
            $data['nclub'],
            is_array($data['natio']) ? null : $data['natio'],
            $data['clglob'],
            $data['point'],
            $data['aclglob'],
            $data['apoint'],
            intval($data['clast']),
            intval($data['categ']),
            intval($data['rangreg']),
            intval($data['rangdep']),
            intval($data['valcla']),
            $data['clpro'],
            intval($data['valinit'])
        );
    }

    /**
     * @param array $data
     * @return ClassementIndividuel[]
     */
    private function classementIndividuelFactory(array $data) : array {
        $result = [];
        foreach ($data as $classement) {
            $result[] = new ClassementIndividuel(
                $classement['rang'],
                $classement['nom'],
                $classement['ctl'],
                $classement['club'],
                $classement['points']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Club[]
     */
    private function clubFactory(array $data) : array {
        $result = [];
        foreach ($data as $clubData){
            $result[] = new Club(
                $clubData['idclub'],
                $clubData['numero'],
                $clubData['nom'],
                is_array($clubData['validation']) ? null : \DateTime::createFromFormat('d/m/Y', $clubData['validation'])
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return ClubDetails
     */
    private function clubDetailsFactory(array $data) : ClubDetails {
        return new ClubDetails(
            intval($data['idclub']),
            intval($data['numero']),
            is_array($data['nomsalle']) ? null : $data['nomsalle'],
            is_array($data['adressesalle1']) ? null : $data['adressesalle1'],
            is_array($data['adressesalle2']) ? null : $data['adressesalle2'],
            is_array($data['adressesalle3']) ? null : $data['adressesalle3'],
            is_array($data['codepsalle']) ? null : $data['codepsalle'],
            is_array($data['villesalle']) ? null : $data['villesalle'],
            is_array($data['web']) ? null : $data['web'],
            is_array($data['nomcor']) ? null : $data['nomcor'],
            is_array($data['prenomcor']) ? null : $data['prenomcor'],
            is_array($data['mailcor']) ? null : $data['mailcor'],
            is_array($data['telcor']) ? null : $data['telcor'],
            is_array($data['latitude']) ? null : floatval($data['latitude']),
            is_array($data['longitude']) ? null : floatval($data['longitude'])
        );
    }

    /**
     * @param array $data
     * @return Division[]
     */
    private function divisionFactory(array $data) : array {
        $result = [];
        foreach ($data as $division) {
            $result[] = new Division(
                $division['iddivision'],
                $division['libelle'],
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Epreuve[]
     */
    private function epreuveFactory(array $data) : array {
        $result = [];
        foreach ($data as $epreuve) {
            $result[] = new Epreuve(
                $epreuve['idepreuve'],
                $epreuve['idorga'],
                is_array($epreuve['libelle']) ? implode(' ', $epreuve['libelle']) : $epreuve['libelle'],
                $epreuve['typepreuve']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Equipe[]
     */
    private function equipeFactory(array $data) : array {
        $result = [];
        foreach ($data as $dataEquipe) {
            $result[] = new Equipe(
                $dataEquipe['libequipe'],
                $dataEquipe['libdivision'],
                is_array($dataEquipe['liendivision']) ? null : $dataEquipe['liendivision'],
                $dataEquipe['idepr'],
                $dataEquipe['libepr']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return EquipePoule[]
     */
    private function equipePouleFactory(array $data) : array {
        $result = [];
        $lastClassment = 0;
        foreach ($data as $equipePouleData) {

            if (!is_string($equipePouleData['equipe'])) {
                continue;
            }

            $result[] = new EquipePoule(
                $equipePouleData['poule'],
                $equipePouleData['clt'] === '-' ? $lastClassment : intval($equipePouleData['clt']),
                $equipePouleData['equipe'],
                intval($equipePouleData['joue']),
                intval($equipePouleData['pts']),
                $equipePouleData['numero'],
                intval($equipePouleData['totvic']),
                intval($equipePouleData['totdef']),
                intval($equipePouleData['idequipe']),
                $equipePouleData['idclub'],
                $equipePouleData['nul'],
                $equipePouleData['pf'],
                $equipePouleData['pg'],
                $equipePouleData['pp']
            );
            $lastClassment = $equipePouleData['clt'] == "-" ? $lastClassment : intval($equipePouleData['clt']);
        }
        return $result;
    }


    /**
     * @param array $data
     * @return PouleClassement[]
     */
    private function pouleClassementFactory(array $data) : array {
        $result = [];
        foreach ($data as $equipePouleData) {
            $result[] = new PouleClassement(
                $equipePouleData['poule'],
                intval($equipePouleData['clt']),
                $equipePouleData['equipe'],
                intval($equipePouleData['joue']),
                intval($equipePouleData['pts']),
                $equipePouleData['numero'],
                intval($equipePouleData['totvic']),
                intval($equipePouleData['totdef']),
                intval($equipePouleData['idequipe']),
                $equipePouleData['idclub'],
                intval($equipePouleData['vic']),
                intval($equipePouleData['def']),
                intval($equipePouleData['nul']),
                intval($equipePouleData['pf']),
                intval($equipePouleData['pg']),
                intval($equipePouleData['pp'])
            );
        }
        return $result;
    }


    /**
     * @param array $data
     * @return Historique[]
     */
    private function historiqueFactory(array $data) : array {
        $result = [];
        foreach ($data as $classement) {
            $explode = explode(' ', $classement['saison']);
            $result[] = new Historique(
                is_array($classement['place']) ? null : $classement['place'],
                $explode[1],
                $explode[3],
                intval($classement['phase']),
                intval($classement['point'])
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Joueur[]
     */
    private function joueurFactory(array $data) : array {
        $result = [];
        foreach ($data as $joueur) {
            $realJoueur = new Joueur(
                $joueur['licence'],
                is_array($joueur['nclub']) ? null : $joueur['nclub'],
                is_array($joueur['club']) ? null : $joueur['club'],
                $joueur['nom'],
                $joueur['prenom'],
                $joueur['clast'] ?? null);
            $result[] = $realJoueur;
        }
        return $result;
    }

    /**
     * @param array $data
     * @return JoueurDetails[]
     */
    private function joueurDetailsFactory(array $data) : array {
        $result = [];
        foreach ($data as $joueur) {
            $result[] = new JoueurDetails(
                $joueur['idlicence'],
                $joueur['licence'],
                $joueur['nom'],
                $joueur['prenom'],
                $joueur['numclub'],
                $joueur['nomclub'],
                $joueur['sexe'] === 'M' ? true : false,
                is_array($joueur['type']) ? null : $joueur['type'],
                is_array($joueur['validation']) ? null : \DateTime::createFromFormat('d/m/Y', $joueur['validation']),
                is_array($joueur['echelon']) ? null : $joueur['echelon'],
                is_array($joueur['place']) ? null : $joueur['place'],
                intval($joueur['point']),
                is_array($joueur['cat']) ? "" : $joueur['cat'],
                intval($joueur['pointm']),
                intval($joueur['apointm']),
                intval($joueur['initm']),
                is_array($joueur['mutation']) ? null : \DateTime::createFromFormat('d/m/Y', $joueur['mutation']),
                is_array($joueur['natio']) ? "F" : $joueur['natio'],
                is_array($joueur['arb']) ? null : $joueur['arb'],
                is_array($joueur['ja']) ? null : $joueur['ja'],
                is_array($joueur['tech']) ? null : $joueur['tech'],
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Organisme[]
     */
    private function organismeFactory(array $data) : array {
        $result = [];
        foreach ($data as $organisme) {
            $result[] = new Organisme(
                $organisme["libelle"],
                $organisme["id"],
                $organisme["code"],
                is_array($organisme["idPere"]) ? null : $organisme['idPere']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Partie[]
     */
    private function partieFactory(array $data) : array {
        $res = [];
        foreach ($data as $partie) {
            list($nom, $prenom) = Utils::returnNomPrenom($partie['advnompre']);
            $res[] = new Partie(
                $partie['licence'],
                $partie["vd"] === "V" ? true : false,
                intval($partie['numjourn']),
                $partie['codechamp'],
                \DateTime::createFromFormat('d/m/Y', $partie['date']),
                floatval($partie['pointres']),
                floatval($partie['coefchamp']),
                $partie['advlic'],
                $partie['advsexe'] === 'M' ? true : false,
                $nom,
                $prenom,
                intval($partie['advclaof'])
            );
        }
        return $res;
    }

    /**
     * @param array $data
     * @return PartieIndividuel[]
     */
    private function partieIndividuelFactory(array $data) : array {
        $result = [];
        foreach ($data as $partie) {
            $result[] = new PartieIndividuel(
                $partie['libelle'],
                $partie['vain'],
                $partie['perd'],
                $partie['forfait']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Poule[]
     */
    private function pouleFactory(array $data) : array {
        $result = [];
        foreach ($data as $dataPoule) {
            $result[] = new Poule(
                $dataPoule['lien'],
                $dataPoule['libelle']
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return PouleResult[]
     */
    private function pouleResultFactory(array $data) : array {
        $result = [];
        foreach ($data as $dataResult) {
            $result[] = new PouleResult(
                $dataResult['libelle'],
                $dataResult['equa'],
                $dataResult['equb'],
                $dataResult['scorea'],
                $dataResult['scoreb'],
                $dataResult['lien'],
                $dataResult['dateprevue'],
                $dataResult['datereelle'],
                $dataResult['ncluba'],
                $dataResult['nclubb'],
                $dataResult['poule'],
                !!$dataResult['live'],
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @return Rencontre[]
     */
    private function rencontreFactory(array $data) : array {
        $result = [];
        foreach ($data as $dataRencontre) {
            if(isset($dataRencontre['equa']) && isset($dataRencontre['equb']))
            {
                $equipeA = $dataRencontre['equa'];
                $equipeB = $dataRencontre['equb'];

                $result[] = new Rencontre(
                    $dataRencontre['libelle'],
                    is_array($equipeA) ? '': $equipeA,
                    is_array($equipeB) ? '': $equipeB,
                    intval($dataRencontre['scorea']),
                    intval($dataRencontre['scoreb']),
                    is_array($dataRencontre['lien']) ? null : $dataRencontre['lien'],
                    \DateTime::createFromFormat('d/m/Y', $dataRencontre['dateprevue']),
                    empty($dataRencontre['datereelle']) ? null : \DateTime::createFromFormat('d/m/Y', $dataRencontre['datereelle'])
                );
            }
        }
        return $result;
    }

    /**
     * @param array $data
     * @param string $clubEquipeA
     * @param string $clubEquipeB
     * @return RencontreDetails
     */
    private function rencontreDetailsFactory(array $data, string $clubEquipeA, string $clubEquipeB): RencontreDetails
    {
        $formatter = new Formatter($this->api);
        $partieService = new PartieService();

        $joueursA = [];
        $joueursB = [];
        foreach ($data['joueur'] as $joueur) {
            $joueursA[] = [$joueur['xja'] ?: '', $joueur['xca'] ?: ''];
            $joueursB[] = [$joueur['xjb'] ?: '', $joueur['xcb'] ?: ''];
        }
        $joueursAFormatted = $formatter->formatJoueurs($joueursA, $clubEquipeA);
        $joueursBFormatted = $formatter->formatJoueurs($joueursB, $clubEquipeB);

        $parties = $partieService->getParties($data['partie']);

        if (!isset($data['resultat']['resa'])) {
            $scores = $partieService->getScores($parties);
            $scoreA = $scores['scoreA'];
            $scoreB = $scores['scoreB'];
        } else {
            $scoreA = $data['resultat']['resa'] == "F0" ? 0 : $data['resultat']['resa'];
            $scoreB = $data['resultat']['resb'] == "F0" ? 0 : $data['resultat']['resb'];
        }

        $expected = $partieService->getExpectedPoints($parties, $joueursAFormatted, $joueursBFormatted);

        return new RencontreDetails(
            $data['resultat']['equa'],
            $data['resultat']['equb'],
            is_array($scoreA) ? 0 : $scoreA,
            is_array($scoreB) ? 0 : $scoreB,
            $joueursAFormatted,
            $joueursBFormatted,
            $parties,
            $expected['expectedA'],
            $expected['expectedB']
        );
    }

    /**
     * @param array $data
     * @return Tour[]
     */
    private function TourFactory(array $data): array
    {
        $result = [];
        foreach ($data as $tour) {
            $result[] = new Tour(
                $tour['libelle'],
                is_array($tour['lien']) ? null : $tour['lien'],
                is_array($tour['date']) ? null : \DateTime::createFromFormat('d/m/Y', $tour['date'])
            );
        }
        return $result;
    }

    /**
     * @param array $data
     * @param Partie[] $validatedParties
     * @return UnvalidatedPartie[]
     */
    private function UnvalidatedPartieFactory(array $data, array $validatedParties): array
    {
        $rencontreService = new RencontreService();

        $result = [];
        foreach ($data as $partie) {
            if ($partie["victoire"] !== "V" || $partie["forfait"] !== "1") {
                list($nom, $prenom) = Utils::returnNomPrenom($partie['nom']);
                $found = count(array_filter($validatedParties, function($validatedPartie) use ($partie, $nom, $prenom) {
                    return $partie["date"] === $validatedPartie->getDate()->format("d/m/Y")
                        and Utils::removeAccentLowerCaseRegex($nom) === Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversaireNom())
                        and (
                            preg_match('/' . Utils::removeAccentLowerCaseRegex($prenom) . '.*/', Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversairePrenom())) or
                            str_contains(Utils::removeAccentLowerCaseRegex($prenom), Utils::removeAccentLowerCaseRegex($validatedPartie->getAdversairePrenom()))
                        );
                }));

                if (!$found and $prenom != "Absent Absent"
                    and $rencontreService->isInCurrentSaison(\DateTime::createFromFormat('d/m/Y', $partie['date']))
                    and $rencontreService->isInCurrentVirtualMonth(\DateTime::createFromFormat('d/m/Y', $partie['date']))) {
                    $result[] = new UnvalidatedPartie(
                        $partie["epreuve"],
                        $partie["idpartie"],
                        floatval($partie["coefchamp"]),
                        $partie["victoire"] === "V",
                        $partie["forfait"] === "1",
                        \DateTime::createFromFormat('d/m/Y', $partie['date']),
                        $nom,
                        $prenom,
                        Utils::formatPoints($partie["classement"])
                    );
                }
            }
        }
        return $result;
    }
}