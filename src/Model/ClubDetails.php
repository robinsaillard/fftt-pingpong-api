<?php


namespace FFTTPingpongApi\Model;


class ClubDetails
{
    /**
     * @var int idClub
     */
    private $idClub;

    /**
     * @var int numero
     */
    private $numero;

    /**
     * @var string|null nomSalle
     */
    private $nomSalle;

    /**
     * @var string|null adresseSalle1
     */
    private $adresseSalle1;

    /**
     * @var string|null adresseSalle2
     */
    private $adresseSalle2;

    /**
     * @var string|null adresseSalle3
     */
    private $adresseSalle3;

    /**
     * @var string|null codePostaleSalle
     */
    private $codePostaleSalle;

    /**
     * @var string|null villeSalle
     */
    private $villeSalle;

    /**
     * @var string|null siteWeb
     */
    private $siteWeb;

    /**
     * @var string|null nomCor
     */
    private $nomCor;

    /**
     * @var string|null prenomCor
     */
    private $prenomCor;

    /**
     * @var string|null mailCor
     */
    private $mailCor;

    /**
     * @var string|null telCor
     */
    private $telCor;

    /**
     * @var float|null latitude
     */
    private $latitude;

    /**
     * @var float|null longitude
     */
    private $longitude;

    /**
     * ClubDetails constructor
     * 
     * @param int $idClub The ID of the club.
     * @param int $numero The parameter "numero" is an integer that represents the number of the club.
     * @param string|null $nomSalle The name of the room or venue.
     * @param string|null $adresseSalle1 The first line of the address of the salle (room) where the club is
     * located.
     * @param string|null $adresseSalle2 The parameter "adresseSalle2" is a nullable string that represents the
     * second line of the address of a room. It is an optional parameter, so it can be null if not
     * provided.
     * @param string|null $adresseSalle3 The parameter "adresseSalle3" is a nullable string that represents the
     * third line of the address of a room. It is an optional parameter, so it can be null if not
     * provided.
     * @param string|null $codePostaleSalle The parameter "codePostaleSalle" is a nullable string that represents
     * the postal code of the venue.
     * @param string|null $villeSalle The parameter "villeSalle" represents the city where the club's salle (room)
     * is located.
     * @param string|null $siteWeb The parameter "siteWeb" is a nullable string that represents the website of the
     * club.
     * @param string|null $nomCor The parameter "nomCor" represents the last name of the contact person for the
     * club.
     * @param string|null $prenomCor The parameter "prenomCor" represents the first name of the contact person for
     * the club.
     * @param string|null $mailCor The parameter "mailCor" is a string that represents the email address of the
     * club's contact person.
     * @param string|null $telCor The parameter "telCor" represents the telephone number of the club's contact
     * person.
     * @param float|null $latitude The latitude of the location of the club's salle (venue).
     * @param float|null $longitude The longitude of the location of the club's salle (venue).
     */
    public function __construct(
        int $idClub,
        int $numero,
        ?string $nomSalle,
        ?string $adresseSalle1,
        ?string $adresseSalle2,
        ?string $adresseSalle3,
        ?string $codePostaleSalle,
        ?string $villeSalle,
        ?string $siteWeb,
        ?string $nomCor,
        ?string $prenomCor,
        ?string $mailCor,
        ?string $telCor,
        ?float $latitude,
        ?float $longitude
    )
    {
        $this->idClub = $idClub;
        $this->numero = $numero;
        $this->nomSalle = $nomSalle;
        $this->adresseSalle1 = $adresseSalle1;
        $this->adresseSalle2 = $adresseSalle2;
        $this->adresseSalle3 = $adresseSalle3;
        $this->codePostaleSalle = $codePostaleSalle;
        $this->villeSalle = $villeSalle;
        $this->siteWeb = $siteWeb;
        $this->nomCor = $nomCor;
        $this->prenomCor = $prenomCor;
        $this->mailCor = $mailCor;
        $this->telCor = $telCor;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getIdClub(): int
    {
        return $this->idClub;
    }

    /**
     * @return int
     */
    public function getNumero(): int
    {
        return $this->numero;
    }

    /**
     * @return string|null
     */
    public function getNomSalle(): ?string
    {
        return $this->nomSalle;
    }

    /**
     * @return string|null
     */
    public function getAdresseSalle1(): ?string
    {
        return $this->adresseSalle1;
    }

    /**
     * @return string|null
     */
    public function getAdresseSalle2(): ?string
    {
        return $this->adresseSalle2;
    }

    /**
     * @return string|null
     */
    public function getAdresseSalle3(): ?string
    {
        return $this->adresseSalle3;
    }

    /**
     * @return string|null
     */
    public function getCodePostaleSalle(): ?string
    {
        return $this->codePostaleSalle;
    }

    /**
     * @return string|null
     */
    public function getVilleSalle(): ?string
    {
        return $this->villeSalle;
    }

    /**
     * @return string|null
     */
    public function getSiteWeb(): ?string
    {
        return $this->siteWeb;
    }

    /**
     * @return string|null
     */
    public function getNomCor(): ?string
    {
        return $this->nomCor;
    }

    /**
     * @return string|null
     */
    public function getPrenomCor(): ?string
    {
        return $this->prenomCor;
    }

    /**
     * @return string|null
     */
    public function getMailCor(): ?string
    {
        return $this->mailCor;
    }

    /**
     * @return string|null
     */
    public function getTelCor(): ?string
    {
        return $this->telCor;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

}