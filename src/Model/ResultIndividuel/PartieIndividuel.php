<?php


namespace FFTTPingpongApi\Model\ResultIndividuel;

class PartieIndividuel
{
    /**
     * @var string $libelle
     */
    private $libelle;

    /**
     * @var string $vainqueur
     */
    private $vainqueur;

    /**
     * @var string $perdant
     */
    private $perdant;

    /**
     * @var string $isForfait
     */
    private $isForfait;

    public function __construct(string $libelle, string $vainqueur, string $perdant, bool $isForfait)
    {
        $this->libelle = $libelle;
        $this->vainqueur = $vainqueur;
        $this->perdant = $perdant;
        $this->isForfait = $isForfait;
    }

    /**
     * Get $libelle
     *
     * @return string
     */ 
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * Get $vainqueur
     *
     * @return string
     */ 
    public function getVainqueur(): string
    {
        return $this->vainqueur;
    }

    /**
     * Get $perdant
     *
     * @return string
     */ 
    public function getPerdant(): string
    {
        return $this->perdant;
    }

    /**
     * Get $isForfait
     *
     * @return string
     */ 
    public function isForfait(): bool
    {
        return $this->isForfait;
    }
}