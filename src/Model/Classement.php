<?php


namespace FFTTPingpongApi\Model;


class Classement
{
    /**
     * @var string $licence
     */
    private $licence;

    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var string $prenom
     */
    private $prenom;

    /**
     * @var string $clubName
     */
    private $clubName;

    /**
     * @var string $clubNumber
     */
    private $clubNumber;

    /**
     * @var string|null $nationalite
     */
    private $nationalite;

    /**
     * @var string $classementGlobal
     */
    private $classementGlobal;

    /**
     * @var float $points
     */
    private $points;

    /**
     * @var string $ancienClassementGlobal
     */
    private $ancienClassementGlobal;

    /**
     * @var float $anciensPoints
     */
    private $anciensPoints;

    /**
     * @var int $classement
     */
    private $classement;

    /**
     * @var string $categorieAge
     */
    private $categorieAge;

    /**
     * @var int $rangRegional
     */
    private $rangRegional;

    /**
     * @var int $rangDepartemental
     */
    private $rangDepartemental;

    /**
     * @var int $pointsOfficiels
     */
    private $pointsOfficiels;

    /**
     * @var string $propositionClassement
     */
    private $propositionClassement;

    /**
     * @var float $pointsInitials
     */
    private $pointsInitials;

    /**
     * Classement constructor
     * 
     * @param string $licence The licence parameter is a string that represents the license number of
     * the player.
     * @param string $nom The parameter "nom" represents the last name of a person.
     * @param string $prenom The parameter "prenom" represents the first name of a person.
     * @param string $clubName The name of the club the player belongs to.
     * @param string $clubNumber The clubNumber parameter is a string that represents the number of the
     * club the player belongs to.
     * @param string|null $nationalite The parameter "nationalite" represents the nationality of a person. It
     * is expected to be a string value.
     * @param string $classementGlobal The parameter "classementGlobal" represents the player's global
     * ranking.
     * @param float $points The "points" parameter is a float value representing the number of points
     * earned by the player.
     * @param string $ancienClassementGlobal The parameter "ancienClassementGlobal" represents the
     * previous global ranking of a player.
     * @param float $anciensPoints The variable "anciensPoints" represents the previous points of the
     * player before the current update.
     * @param int $classement The parameter "classement" represents the player's ranking or position in
     * a particular category or competition.
     * @param string $categorieAge The parameter "categorieAge" represents the age category of the
     * player. It is expected to be a string value.
     * @param int $rangRegional The parameter "rangRegional" represents the regional ranking of the
     * player. It is an integer value that indicates the player's position in the regional ranking.
     * @param int $rangDepartemental The parameter "rangDepartemental" represents the regional ranking
     * of a player in a specific department or region. It is an integer value that indicates the
     * player's position or rank within their department or region.
     * @param int $pointsOfficiels The parameter "pointsOfficiels" represents the official points of the
     * player. It is an integer value.
     * @param string $propositionClassement The parameter `propositionClassement` is a string that
     * represents the proposed classification for a player. It is used to suggest a new ranking or
     * classification based on certain criteria or calculations.
     * @param float $pointsInitials The parameter "pointsInitials" is a float value representing the
     * initial points of a player.
     */
    public function __construct(
        string $licence,
        string $nom,
        string $prenom,
        string $clubName,
        string $clubNumber,
        ?string $nationalite,
        string $classementGlobal,
        float $points,
        string $ancienClassementGlobal,
        float $anciensPoints,
        int $classement,
        string $categorieAge,
        int $rangRegional,
        int $rangDepartemental,
        int $pointsOfficiels,
        string $propositionClassement,
        float $pointsInitials
    )
    {
        $this->licence = $licence;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->clubName = $clubName;
        $this->clubNumber = $clubNumber;
        $this->nationalite = $nationalite;
        $this->classementGlobal = $classementGlobal;
        $this->points = $points;
        $this->ancienClassementGlobal = $ancienClassementGlobal;
        $this->anciensPoints = $anciensPoints;
        $this->classement = $classement;
        $this->categorieAge = $categorieAge;
        $this->rangRegional = $rangRegional;
        $this->rangDepartemental = $rangDepartemental;
        $this->pointsOfficiels = $pointsOfficiels;
        $this->pointsInitials = $pointsInitials;
        $this->propositionClassement = $propositionClassement;
    }

    /**
     * @return float
     */
    public function getPoints(): float
    {
        return $this->points;
    }

    /**
     * @return float
     */
    public function getAnciensPoints(): float
    {
        return $this->anciensPoints;
    }

    /**
     * @return int
     */
    public function getClassement(): int
    {
        return $this->classement;
    }

    /**
     * @return int
     */
    public function getRangRegional(): int
    {
        return $this->rangRegional;
    }

    /**
     * @return int
     */
    public function getRangDepartemental(): int
    {
        return $this->rangDepartemental;
    }

    /**
     * @return int
     */
    public function getPointsOfficiels(): int
    {
        return $this->pointsOfficiels;
    }

    /**
     * @return float
     */
    public function getPointsInitials(): float
    {
        return $this->pointsInitials;
    }


    /**
     * Get $nom
     *
     * @return string
     */ 
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Get $prenom
     *
     * @return string
     */ 
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Get $clubNumber
     *
     * @return string
     */ 
    public function getClubNumber(): string
    {
        return $this->clubNumber;
    }

    /**
     * Get $nationalite
     *
     * @return string|null
     */ 
    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    /**
     * Get $classementGlobal
     *
     * @return string
     */ 
    public function getClassementGlobal(): string
    {
        return $this->classementGlobal;
    }

    /**
     * Get $ancienClassementGlobal
     *
     * @return string
     */ 
    public function getAncienClassementGlobal(): string
    {
        return $this->ancienClassementGlobal;
    }

    /**
     * Get $categorieAge
     *
     * @return string
     */ 
    public function getCategorieAge(): string
    {
        return $this->categorieAge;
    }

    /**
     * Get $propositionClassement
     *
     * @return string
     */ 
    public function getPropositionClassement(): string
    {
        return $this->propositionClassement;
    }

    /**
     * Get $clubName
     *
     * @return string
     */ 
    public function getClubName(): string
    {
        return $this->clubName;
    }

    /**
     * Get $licence
     *
     * @return string
     */ 
    public function getLicence(): string
    {
        return $this->licence;
    }
}