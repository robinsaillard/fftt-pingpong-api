<?php

namespace FFTTPingpongApi\Model\Poule;

class PouleClassement
{
    /**
     * @var string $poule
     */
    private $poule;

    /**
     * @var int $clt
     */
    private $clt;

    /**
     * @var string $equipe
     */
    private $equipe;

    /**
     * @var int $joue
     */
    private $joue;

    /**
     * @var int $pts
     */
    private $pts;

    /**
     * @var string $numero
     */
    private $numero;

    /**
     * @var int $totvic
     */
    private $totvic;

    /**
     * @var int $totdef
     */
    private $totdef;

    /**
     * @var int $idequipe
     */
    private $idequipe;

    /**
     * @var string $idclub
     */
    private $idclub;

    /**
     * @var int $vic
     */
    private $vic;

    /**
     * @var int $def
     */
    private $def;

    /**
     * @var int $nul
     */
    private $nul;

    /**
     * @var int $pf
     */
    private $pf;

    /**
     * @var int $pg
     */
    private $pg;

    /**
     * @var int $pp
     */
    private $pp;


    /**
    * Poule classement constructor
    * @param string $poule
    * @param int $clt
    * @param string $equipe
    * @param int $joue
    * @param int $pts
    * @param string $numero
    * @param int $totvic
    * @param int $totdef
    * @param int $idequipe
    * @param string $idclub
    * @param int $vic
    * @param int $def
    * @param int $nul
    * @param int $pf
    * @param int $pg
    * @param int $pp
    */
    public function __construct(
        string $poule,
        int $clt,
        string $equipe,
        int $joue,
        int $pts,
        string $numero,
        int $totvic,
        int $totdef,
        int $idequipe,
        string $idclub,
        int $vic,
        int $def,
        int $nul,
        int $pf,
        int $pg,
        int $pp
    )
    {
        $this->poule = $poule;
        $this->clt = $clt;
        $this->equipe = $equipe;
        $this->joue = $joue;
        $this->pts = $pts;
        $this->numero = $numero;
        $this->totvic = $totvic;
        $this->totdef = $totdef;
        $this->idequipe = $idequipe;
        $this->idclub = $idclub;
        $this->vic = $vic;
        $this->def = $def;
        $this->nul = $nul;
        $this->pf = $pf;
        $this->pg = $pg;
        $this->pp = $pp;
    }

    /**
     * @return string
     */
    public function getPoule(): string
    {
        return $this->poule;
    }

    /**
     * @return int
     */
    public function getClt(): int
    {
        return $this->clt;
    }

    /**
     * @return string
     */
    public function getEquipe(): string
    {
        return $this->equipe;
    }

    /**
     * @return int
     */
    public function getJoue(): int
    {
        return $this->joue;
    }

    /**
     * @return int
     */
    public function getPts(): int
    {
        return $this->pts;
    }

    /**
     * @return string
     */
    public function getNumero(): string
    {
        return $this->numero;
    }

    /**
     * @return int
     */
    public function getTotvic(): int
    {
        return $this->totvic;
    }

    /**
     * @return int
     */
    public function getTotdef(): int
    {
        return $this->totdef;
    }

    /**
     * @return int
     */
    public function getIdequipe(): int
    {
        return $this->idequipe;
    }

    /**
     * @return string
     */
    public function getIdclub(): string
    {
        return $this->idclub;
    }

    /**
     * @return int
     */
    public function getVic(): int
    {
        return $this->vic;
    }

    /**
     * @return int
     */
    public function getDef(): int
    {
        return $this->def;
    }

    /**
     * @return int
     */
    public function getNul(): int
    {
        return $this->nul;
    }

    /**
     * @return int
     */
    public function getPf(): int
    {
        return $this->pf;
    }

    /**
     * @return int
     */
    public function getPg(): int
    {
        return $this->pg;
    }

    /**
     * @return int
     */
    public function getPp(): int
    {
        return $this->pp;
    }

}