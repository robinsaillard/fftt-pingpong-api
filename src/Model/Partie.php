<?php


namespace FFTTPingpongApi\Model;


class Partie
{
    /**
     * @var string $licenceId
     */
    private $licenceId;

    /**
     * @var bool $isVictoire
     */
    private $isVictoire;

    /**
     * @var int $journee
     */
    private $journee;

    /**
     * @var string $codeChampionnat
     */
    private $codeChampionnat;

    /**
     * @var \DateTime $date
     */
    private $date;

    /**
     * @var float $pointsObtenus
     */
    private $pointsObtenus;

    /**
     * @var float $coefficient
     */
    private $coefficient;

    /**
     * @var string $adversaireLicence
     */
    private $adversaireLicence;

    /**
     * @var bool $adversaireIsHomme
     */
    private $adversaireIsHomme;

    /**
     * @var string $adversaireNom
     */
    private $adversaireNom;

    /**
     * @var string $adversairePrenom
     */
    private $adversairePrenom;

    /**
     * @var int $adversaireClassement
     */
    private $adversaireClassement;

    /**
     * Partie constructor
     * 
     * @param bool $isVictoire A boolean value indicating whether the match was a victory or not.
     * @param int $journee The parameter "journee" represents the round or matchday number of the game.
     * @param \DateTime $date The "date" parameter represents the date of the
     * match.
     * @param float $pointsObtenus The parameter "pointsObtenus" represents the number of points
     * obtained in a match.
     * @param float $coefficient The coefficient is a floating-point number that represents the weight
     * or importance of the match. It is used to calculate the number of points obtained in the match.
     * @param string $adversaireLicence The parameter "adversaireLicence" is a string that represents
     * the license of the opponent.
     * @param bool $adversaireIsHomme This parameter is a boolean value that indicates whether the
     * opponent is a male or not.
     * @param string $adversaireNom The parameter "adversaireNom" represents the last name of the
     * opponent.
     * @param string $adversairePrenom The parameter "adversairePrenom" represents the first name of the
     * opponent.
     * @param int $adversaireClassement The parameter "adversaireClassement" represents the ranking of
     * the opponent. It is an integer value that indicates the position or rank of the opponent in a
     * particular context, such as a sports competition or a leaderboard.
     */
    public function __construct(
        string $licenceId,
        bool $isVictoire,
        int $journee,
        string $codeChampionnat,
        \DateTime $date,
        float $pointsObtenus,
        float $coefficient,
        string $adversaireLicence,
        bool $adversaireIsHomme,
        string $adversaireNom,
        string $adversairePrenom,
        int $adversaireClassement
    )
    {
        $this->licenceId = $licenceId;
        $this->isVictoire = $isVictoire;
        $this->journee = $journee;
        $this->codeChampionnat = $codeChampionnat;
        $this->date = $date;
        $this->pointsObtenus = $pointsObtenus;
        $this->coefficient = $coefficient;
        $this->adversaireLicence = $adversaireLicence;
        $this->adversaireIsHomme = $adversaireIsHomme;
        $this->adversaireNom = $adversaireNom;
        $this->adversairePrenom = $adversairePrenom;
        $this->adversaireClassement = $adversaireClassement;
    }

    /**
     * @return bool
     */
    public function isIsVictoire(): bool
    {
        return $this->isVictoire;
    }

    /**
     * @return int
     */
    public function getJournee(): int
    {
        return $this->journee;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getPointsObtenus(): float
    {
        return $this->pointsObtenus;
    }

    /**
     * @return float
     */
    public function getCoefficient(): float
    {
        return $this->coefficient;
    }

    /**
     * @return string
     */
    public function getAdversaireLicence(): string
    {
        return $this->adversaireLicence;
    }

    /**
     * @return bool
     */
    public function isAdversaireIsHomme(): bool
    {
        return $this->adversaireIsHomme;
    }

    /**
     * @return string
     */
    public function getAdversaireNom(): string
    {
        return $this->adversaireNom;
    }

    /**
     * @return string
     */
    public function getAdversairePrenom(): string
    {
        return $this->adversairePrenom;
    }

    /**
     * @return int
     */
    public function getAdversaireClassement(): int
    {
        return $this->adversaireClassement;
    }

    /**
     * Get $licenceId
     *
     * @return string
     */ 
    public function getLicenceId(): string
    {
        return $this->licenceId;
    }

    /**
     * Get $codeChampionnat
     *
     * @return string
     */ 
    public function getCodeChampionnat(): string
    {
        return $this->codeChampionnat;
    }
}

