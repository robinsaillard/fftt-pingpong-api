<?php


namespace FFTTPingpongApi;

use GuzzleHttp\Client;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use FFTTPingpongApi\Exception\NoFFTTResponseException;

use FFTTPingpongApi\Exception\UnauthorizedCredentials;
use FFTTPingpongApi\Exception\URIPartNotValidException;
use FFTTPingpongApi\Exception\InvalidURIParametersException;

const FFTTURL = 'http://www.fftt.com/mobile/pxml/';

class ApiRequest
{
    private $password;
    private $id;

    public function __construct(string $password, string $id)
    {
        $this->password = $password;
        $this->id = $id;
    }

    private function prepare(string $request, array $params = [], string $queryParameter = null) : string{
        $time = round(microtime(true)*1000);
        $timeCrypted = hash_hmac("sha1", $time, $this->password);
        $uri =  FFTTURL.$request.'.php?serie='.$this->id.'&tm='.$time.'&tmc='.$timeCrypted.'&id='.$this->id;
        if($queryParameter){
            $uri.= "&".$queryParameter;
        }
        foreach ($params as $key => $value){
            $uri .= '&'.$key.'='.$value;
        }
        return $uri;
    }

    public function send(string $uri){
        $client = new Client([
            'verify' => false
        ]);
        $response = $client->request('GET', $uri);
        if($response->getStatusCode() !== 200){
            throw new \DomainException("Request ".$uri." returns an error");
        }
        $content = $response->getBody()->getContents();

        $content = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $content);
        $content = preg_replace('/ISO-8859-1/', 'utf-8', $content);
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');

        $content = html_entity_decode($content);

        libxml_use_internal_errors(true);
        $dom = new \DOMDocument("1.0", "UTF-8");
        $dom->strictErrorChecking = false;
        $dom->validateOnParse = false;
        $dom->recover = true;
        $dom->loadXML($content);
        $xml = simplexml_import_dom($dom);

        $xml = simplexml_load_string($xml->asXML(), "SimpleXMLElement", LIBXML_NOCDATA);

        libxml_clear_errors();
        libxml_use_internal_errors(false);

        return json_decode(json_encode($xml), true);
    }


    public function get(string $request, array $params = [], string $queryParameter = null){

        $chaine = $this->prepare($request, $params, $queryParameter);
        try{
            $result = $this->send($chaine);
        }
        catch (ClientException $ce){
            if($ce->getResponse()->getStatusCode() === 401){
                throw new UnauthorizedCredentials($request, $ce->getResponse()->getBody()->getContents());
            }
            throw new URIPartNotValidException($request);
        }
        catch (ServerException $se) {
            if ($se->getResponse()->getStatusCode() === 500) {
                $errorMessage = $se->getResponse()->getBody()->getContents();
                error_log("Erreur 500 sur la requête API : " . $errorMessage);
                throw new URIPartNotValidException($request);
            }
            throw $se;
        }

        if(!is_array($result)){
            throw new InvalidURIParametersException($request, $params);
        }
        if(array_key_exists('0', $result)){
            throw new NoFFTTResponseException($chaine);
        }

        return $result;
    }
}