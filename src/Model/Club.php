<?php


namespace FFTTPingpongApi\Model;


class Club
{
    /**
     * @var string $idClub
     */
    private $idClub;

    /**
     * @var string $numero
     */
    private $numero;

    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var \DateTime|null $dateValidation
     */
    private $dateValidation;

    /**
     * Club constructor
     * 
     * @param string $id The id parameter is a string that represents the club's ID. It is used to
     * uniquely identify the club.
     * @param string $numero The "numero" parameter is a string that represents the club's number.
     * @param string $nom The "nom" parameter is a string that represents the name of the club.
     * @param \DateTime|null $dateValidation The parameter `dateValidation` represents the date of validation for a club.
     */
    public function __construct(string $id, string $numero, string $nom, ?\DateTime $dateValidation)
    {
        $this->idClub = $id;
        $this->numero = $numero;
        $this->nom = $nom;
        $this->dateValidation = $dateValidation;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->idClub;
    }

    /**
     * @return string
     */
    public function getNumero(): string
    {
        return $this->numero;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateValidation(): ?\DateTime
    {
        return $this->dateValidation;
    }

}