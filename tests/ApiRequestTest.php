<?php


namespace FFTTPingpongApi\Tests;


use Dotenv\Dotenv;

use FFTTPingpongApi\ApiRequest;

use PHPUnit\Framework\TestCase;
use FFTTPingpongApi\Exception\URIPartNotValidException;

class ApiRequestTest extends TestCase
{
    public function testGetWithBadUriPart(){
        $this->expectException(URIPartNotValidException::class);

        $request = $this->getApiRequest();
        $request->get('hello');
    }

    private function getApiRequest(){
        return new ApiRequest(md5(getenv("FFTT_PASSWORD")), getenv("FFTT_ID"));
    }

    // public function testGetWithGoodUriPartAndBadParameters()
    // {
    //     $this->expectException(InvalidURIParametersException::class);
    //     $request = $this->getApiRequest();
    //     $request->get('xml_joueur', [
    //         'badKey' => 'value'
    //     ]);
    // }

}