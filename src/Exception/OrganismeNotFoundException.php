<?php


namespace FFTTPingpongApi\Exception;


class OrganismeNotFoundException extends \Exception
{
    public function __construct($organisme)
    {
        parent::__construct(
            sprintf(
                "L'organisme '%s' n'existe pas.",
                $organisme
            )
        );
    }
}