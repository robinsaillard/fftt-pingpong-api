<?php

namespace FFTTPingpongApi\Model\Enums;

enum EquipeGenderType: string implements EnumInterface
{
    case MASCULIN = 'M';
    case FEMININ = 'F';
    case MASCULIN_FEMININ = 'A';
    case AUTRE = '';

    /**
     * @return string
     */
    public static function getDefaultType(): string
    {
        return self::MASCULIN->value;
    }

    /**
     * Check if type exists is enum
     *
     * @param string $type
     * @return bool
     */
    public static function isExisting(string $type): bool
    {
        return AbstractEnum::isExisting(self::cases(), $type);
    }
}
?>
