<?php


namespace FFTTPingpongApi\Model;


class UnvalidatedPartie
{
    /**
     * @var string $epreuve
     */
    private $epreuve;

    /**
     * @var string $idPartie
     */
    private $idPartie;

    /**
     * @var float $coefficientChampionnat
     */
    private $coefficientChampionnat;

    /**
     * @var bool $isVictoire
     */
    private $isVictoire;

    /**
     * @var bool $isForfait
     */
    private $isForfait;

    /**
     * @var \DateTime $date
     */
    private $date;

    /**
     * @var string $adversaireNom
     */
    private $adversaireNom;

    /**
     * @var string $adversairePrenom
     */
    private $adversairePrenom;

    /**
     * @var int $adversaireClassement
     */
    private $adversaireClassement;

    /**
     * UnvalidatedPartie constructor
     * 
     * @param string $epreuve The epreuve parameter is a string that represents the type of event or
     * competition.
     * @param string $idPartie The idPartie parameter is a string that represents the unique identifier
     * for the game or match.
     * @param float $coefficientChampionnat The coefficientChampionnat parameter is a float value that
     * represents the coefficient of the championship. It is used to calculate the points earned or
     * lost in a game based on the result and the level of the opponent.
     * @param bool $isVictoire This parameter is a boolean value that indicates whether the game was a
     * victory or not.
     * @param bool $isForfait A boolean value indicating whether the game was a forfeit or not.
     * @param \DateTime $date The "date" parameter represents the date of the game or event.
     * @param string $adversaireNom The parameter "adversaireNom" is a string that represents the last
     * name of the opponent.
     * @param string $adversairePrenom The parameter "adversairePrenom" represents the first name of the
     * opponent.
     * @param int $adversaireClassement The parameter "adversaireClassement" represents the ranking of
     * the opponent in the competition.
     */
    public function __construct(
        string $epreuve,
        string $idPartie,
        float $coefficientChampionnat,
        bool $isVictoire,
        bool $isForfait,
        \DateTime $date,
        string $adversaireNom,
        string $adversairePrenom,
        int $adversaireClassement
    )
    {
        $this->isVictoire = $isVictoire;
        $this->idPartie = $idPartie;
        $this->isForfait = $isForfait;
        $this->date = $date;
        $this->adversaireNom = $adversaireNom;
        $this->adversairePrenom = $adversairePrenom;
        $this->adversaireClassement = $adversaireClassement;
        $this->coefficientChampionnat = $coefficientChampionnat;
        $this->epreuve = $epreuve;
    }

    /**
     * @return bool
     */
    public function isVictoire(): bool
    {
        return $this->isVictoire;
    }

    /**
     * @return bool
     */
    public function isForfait(): bool
    {
        return $this->isForfait;
    }

    /**
     * @return string
     */
    public function getEpreuve(): string
    {
        return $this->epreuve;
    }

    /**
     * @return string
     */
    public function getIdPartie(): string
    {
        return $this->idPartie;
    }

    /**
     * @return float
     */
    public function getCoefficientChampionnat(): float
    {
        return $this->coefficientChampionnat;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getAdversaireNom(): string
    {
        return $this->adversaireNom;
    }

    /**
     * @return string
     */
    public function getAdversairePrenom(): string
    {
        return $this->adversairePrenom;
    }

    /**
     * @return int
     */
    public function getAdversaireClassement(): int
    {
        return $this->adversaireClassement;
    }
}
