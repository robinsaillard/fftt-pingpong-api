<?php

namespace FFTTPingpongApi\Model\Enums;

enum EquipeResultType: string implements EnumInterface
{
    case POULE = 'poule';
    case CLASSEMENT = 'classement';
    case INITIAL = 'initial';
    case RENCONTRES = '';

    /**
     * @return string
     */
    public static function getDefaultType(): string
    {
        return self::POULE->value;
    }

    /**
     * Check if type exists is enum
     *
     * @param string $type
     * @return bool
     */
    public static function isExisting(string $type): bool
    {
        return AbstractEnum::isExisting(self::cases(), $type);
    }
}
?>
