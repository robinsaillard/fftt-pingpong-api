<?php


namespace FFTTPingpongApi\Model\ResultIndividuel;

class ClassementIndividuel
{
    /**
     * @var string $rang
     */
    private $rang;

    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var string $classement
     */
    private $classement;

    /**
     * @var string $club
     */
    private $club;

    /**
     * @var int $points
     */
    private $points;
    
    /**
     * ClassementIndividuel constructor
     * 
     * @param string rang The "rang" parameter is a string that represents the rank of the
     * player in a ranking system. It could be a number or any other identifier that indicates the
     * player's position in a list or table.
     * @param string nom The parameter "nom" represents the name of a player.
     * @param string classement The "classement" parameter is a string that represents the ranking of a
     * player or team in a competition. It could be a numerical value or a descriptive term such as
     * "first", "second", etc.
     * @param string club The "club" parameter represents the name of the club or team that the player
     * belongs to.
     * @param int points The "points" parameter is an integer that represents the number of points
     * earned by the player.
     */
    public function __construct(string $rang, string $nom, string $classement, string $club, int $points)
    {
        $this->rang = $rang;
        $this->nom = $nom;
        $this->classement = $classement;
        $this->club = $club;
        $this->points = $points;
    }
    

    /**
     * Get $rang
     *
     * @return string
     */ 
    public function getRang(): string
    {
        return $this->rang;
    }

    /**
     * Get $nom
     *
     * @return string
     */ 
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Get $classement
     *
     * @return string
     */ 
    public function getClassement(): string
    {
        return $this->classement;
    }

    /**
     * Get $club
     *
     * @return string
     */ 
    public function getClub(): string
    {
        return $this->club;
    }

    /**
     * Get $points
     *
     * @return int
     */ 
    public function getPoints(): int
    {
        return $this->points;
    }
}