<?php


namespace FFTTPingpongApi\Exception;


class InvalidURIParametersException extends \Exception
{
    public function __construct(string $uriPart, array $params)
    {
        $message = sprintf(
            "Les paramètres '%s' ne sont pas valides pour l'URI '%s'",
            implode(', ', array_keys($params)),
            $uriPart
        );
        parent::__construct($message);
    }
}