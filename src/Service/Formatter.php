<?php

namespace FFTTPingpongApi\Service;

use FFTTPingpongApi\Exception\NoFFTTResponseException;
use FFTTPingpongApi\FFTTApi;
use FFTTPingpongApi\Model\Rencontre\Joueur;

class Formatter
{
    /**
     * @var FFTTApi $api
     */
    private FFTTApi $api;

    /**
     * EntityFactory constructor
     * 
     * @param FFTTApi $api The "api" parameter is an instance of the FFTTApi class. It is being injected
     * into the constructor of the current class.
     */
    public function __construct(FFTTApi $api)
    {
        $this->api = $api;
    }
    
    /**
     * @param array $data
     * @param string $playerClubId
     * @return array<string, Joueur>
     */
    public function formatJoueurs(array $data, string $playerClubId): array
    {
        $joueursClub = $this->api->getJoueursByClubFromSpid($playerClubId);

        $joueurs = [];
        foreach ($data as $joueurData) {
            $nomPrenom = $joueurData[0];
            [$nom, $prenom] = Utils::returnNomPrenom($nomPrenom);
            $joueurs[$nomPrenom] = $this->formatJoueur($prenom, $nom, $joueurData[1], $joueursClub);
        }
        return $joueurs;
    }

    /**
     * @param string $prenom
     * @param string $nom
     * @param string $points
     * @param array $joueursClub
     * @return Joueur
     */
    public function formatJoueur(string $prenom, string $nom, string $points, array $joueursClub): Joueur
    {
        if ($nom === "" && $prenom === "Absent") {
            return new Joueur($nom, $prenom, "", null, null);
        }

        try {
            foreach ($joueursClub as $joueurClub) {
                if ($joueurClub->getNom() === Accentuation::remove($nom) && $joueurClub->getPrenom() === $prenom) {

                    $return = preg_match('/^(N.[0-9]*- ){0,1}(?<sexe>[A-Z]{1}) (?<points>[0-9]+)pts$/', $points, $result);

                    if ($return === false) {
                        throw new \RuntimeException(
                            sprintf(
                                "Not able to extract sexe and points in '%s'",
                                $points
                            )
                        );
                    }
                    $sexe = $result['sexe'];
                    $playerPoints = (int) $result['points'];

                    return new Joueur(
                        $joueurClub->getNom(),
                        $joueurClub->getPrenom(),
                        $joueurClub->getLicence(),
                        $playerPoints,
                        $sexe
                    );
                }
            }

        } catch (NoFFTTResponseException $e) {
        }

        return new Joueur($nom, $prenom, "", null, null);
    }
}