<?php


namespace FFTTPingpongApi\Model;


class JoueurDetails
{
    /**
     * @var string $licenceId
     */
    private $licenceId;

    /**
     * @var string $licenceNumber
     */
    private $licenceNumber;

    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var string $prenom
     */
    private $prenom;

    /**
     * @var string $numClub
     */
    private $numClub;

    /**
     * @var string $nomClub
     */
    private $nomClub;

    /**
     * @var bool $isHomme
     */
    private $isHomme;

    /**
     * @var string|null $typeLicence
     */
    private $typeLicence;

    /**
     * @var \DateTime|null $validation
     */
    private $validation;

    /**
     * @var string|null $echelon
     */
    private $echelon;

    /**
     * @var string|null $place
     */
    private $place;

    /**
     * @var float $points
     */
    private $points;

    /**
     * @var string $categorieAge
     */
    private $categorieAge;

    /**
     * @var float $pointsMensuel
     */
    private $pointsMensuel;

    /**
     * @var float $pointsMensuelAnciens
     */
    private $pointsMensuelAnciens;

    /**
     * @var float $valeurInitiale
     */
    private $valeurInitiale;

    /**
     * @var \DateTime|null $mutation
     */
    private $mutation;

    /**
     * @var string $nationalite
     */
    private $nationalite;

    /**
     * @var string|null $gradeArbitre
     */
    private $gradeArbitre;

    /**
     * @var string|null $gradeJugeArbitre
     */
    private $gradeJugeArbitre;

    /**
     * @var string|null $gradeTechnicien
     */
    private $gradeTechnicien;

    /**
     * JoueurDetails constructor
     * 
     * @param string $licenceId The unique identifier for the licence.
     * @param string $licenceNumber The number for the licence.
     * @param string $nom The parameter "nom" represents the last name of a person.
     * @param string $prenom The parameter "prenom" represents the first name of a person.
     * @param string $numClub The parameter "numClub" is a string that represents the number of the
     * club.
     * @param string $nomClub The parameter "nomClub" represents the name of the club to which the
     * person belongs.
     * @param bool $isHomme A boolean value indicating whether the person is male or not.
     * @param string|null $typeLicence The type of licence the person has. It is a string like "T" or "P".
     * @param \DateTime|null $validation The "validation" parameter is a DateTime object that represents the
     * date when the licence was validated.
     * @param string|null $echelon The "echelon" parameter represents the level or rank of the person. It
     * could be used to indicate their skill level or position within a hierarchy.
     * @param string|null $place The "place" parameter represents the position or rank of the person in a
     * competition or event. It could be a numerical value or a string indicating the position, such as
     * "1st", "2nd", "3rd", etc.
     * @param float $points The "points" parameter is a float value representing the number of points
     * earned by the player.
     * @param string $categorieAge The parameter "categorieAge" represents the age category of the
     * person. It is a string that indicates the age group or category that the person belongs to.
     * @param float $pointsMensuel The parameter "pointsMensuel" represents the monthly points earned by
     * the player.
     * @param float $pointsMensuelAnciens The parameter "pointsMensuelAnciens" represents the monthly
     * points earned by the player in the previous season.
     * @param float $valeurInitiale The parameter "valeurInitiale" represents the initial value of a
     * certain variable or property. It is of type float, which means it can hold decimal values.
     * @param \DateTime|null $mutation The "mutation" parameter is of type \DateTime and represents the date
     * of mutation for the object being constructed.
     * @param string|null $gradeArbitre
     * @param string|null $gradeJugeArbitre
     * @param string|null $gradeTechnicien
     */
    public function __construct(
        string $licenceId,
        string $licenceNumber,
        string $nom,
        string $prenom,
        string $numClub,
        string $nomClub,
        bool $isHomme,
        ?string $typeLicence,
        ?\DateTime $validation,
        ?string $echelon,
        ?string $place,
        float $points,
        string $categorieAge,
        float $pointsMensuel,
        float $pointsMensuelAnciens,
        float $valeurInitiale,
        ?\DateTime $mutation,
        string $nationalite,
        ?string $gradeArbitre,
        ?string $gradeJugeArbitre,
        ?string $gradeTechnicien
    )
    {
        $this->licenceId = $licenceId;
        $this->licenceNumber = $licenceNumber;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->numClub = $numClub;
        $this->nomClub = $nomClub;
        $this->isHomme = $isHomme;
        $this->typeLicence = $typeLicence;
        $this->validation = $validation;
        $this->echelon = $echelon;
        $this->place = $place;
        $this->points = $points;
        $this->categorieAge = $categorieAge;
        $this->pointsMensuel = $pointsMensuel;
        $this->pointsMensuelAnciens = $pointsMensuelAnciens;
        $this->valeurInitiale = $valeurInitiale;
        $this->mutation = $mutation;
        $this->nationalite = $nationalite;
        $this->gradeArbitre = $gradeArbitre;
        $this->gradeJugeArbitre = $gradeJugeArbitre;
        $this->gradeTechnicien = $gradeTechnicien;
    }

    /**
     * @return string
     */
    public function getLicenceId(): string
    {
        return $this->licenceId;
    }

    /**
     * @return string
     */
    public function getLicenceNumber(): string
    {
        return $this->licenceNumber;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @return string
     */
    public function getNumClub(): string
    {
        return $this->numClub;
    }

    /**
     * @return string
     */
    public function getNomClub(): string
    {
        return $this->nomClub;
    }

    /**
     * @return bool
     */
    public function isIsHomme(): bool
    {
        return $this->isHomme;
    }

    /**
     * @return string|null
     */
    public function getTypeLicence(): ?string
    {
        return $this->typeLicence;
    }

    /**
     * @return float
     */
    public function getPointsMensuel(): float
    {
        return $this->pointsMensuel;
    }

    /**
     * @return float
     */
    public function getPointsMensuelAnciens(): float
    {
        return $this->pointsMensuelAnciens;
    }


    /**
     * Get $validation
     *
     * @return \DateTime|null
     */ 
    public function getValidation(): ?\DateTime
    {
        return $this->validation;
    }

    /**
     * Get $echelon
     *
     * @return string|null
     */ 
    public function getEchelon(): ?string
    {
        return $this->echelon;
    }

    /**
     * Get $place
     *
     * @return string|null
     */ 
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * Get $points
     *
     * @return float
     */ 
    public function getPoints(): float
    {
        return $this->points;
    }

    /**
     * Get $categorieAge
     *
     * @return string
     */ 
    public function getCategorieAge(): string
    {
        return $this->categorieAge;
    }

    /**
     * Get $valeurInitiale
     *
     * @return float
     */ 
    public function getValeurInitiale(): float
    {
        return $this->valeurInitiale;
    }

    /**
     * Get $mutation
     *
     * @return \DateTime|null
     */ 
    public function getMutation(): ?\DateTime
    {
        return $this->mutation;
    }

    /**
     * Get $nationalite
     *
     * @return string
     */ 
    public function getNationalite(): string
    {
        return $this->nationalite;
    }

    /**
     * Get $gradeArbitre
     *
     * @return string|null
     */ 
    public function getGradeArbitre(): ?string
    {
        return $this->gradeArbitre;
    }

    /**
     * Get $gradeJugeArbitre
     *
     * @return string|null
     */ 
    public function getGradeJugeArbitre(): ?string
    {
        return $this->gradeJugeArbitre;
    }

    /**
     * Get $gradeTechnicien
     *
     * @return string|null
     */ 
    public function getGradeTechnicien(): ?string
    {
        return $this->gradeTechnicien;
    }
}