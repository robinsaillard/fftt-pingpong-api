<?php


namespace FFTTPingpongApi\Model\ResultIndividuel;

class Tour
{
    /**
     * @var string $groupLibelle
     */
    private $groupLibelle;

    /**
     * @var string|null $lien
     */
    private $lien;

    /**
     * @var \DateTime|null $date
     */
    private $date;
    
    /**
     * Tour constructor
     * 
     * @param string groupLibelle A string representing the label or name of a group.
     * @param string|null lien The "lien" parameter is a string that represents a link or URL. It is used to
     * store the link associated with an object.
     * @param \DateTime|null date The `date` parameter represents a specific date
     * and time.
     */
    public function __construct(string $groupLibelle, ?string $lien, ?\DateTime $date)
    {
        $this->groupLibelle = $groupLibelle;
        $this->lien = $lien;
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getGroupLibelle(): string
    {
        return $this->groupLibelle;
    }

    /**
     * @return string|null
     */
    public function getLien(): ?string
    {
        return $this->lien;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }
}