<?php
/**
 * Created by Stephen Sakovitch.
 */

namespace FFTTPingpongApi\Model;


class VirtualPoints
{
    /**
     * @var float $seasonlyPointsWon
     */
    private $seasonlyPointsWon;

    /**
     * @var float $monthlyPointsWon
     */
    private $monthlyPointsWon;

    /**
     * @var float $virtualPoints
     */
    private $virtualPoints;

    /**
     * VirtualPoints constructor.
     * 
     * @param float $monthlyPointsWon This parameter represents the number of points won by a player in
     * a given month.
     * @param float $virtualPoints The virtualPoints parameter represents the amount of virtual points
     * that a player has.
     * @param float $seasonlyPointsWon The parameter "seasonlyPointsWon" represents the total points won
     * by a player in a season.
     */
    public function __construct(float $monthlyPointsWon, float $virtualPoints, float $seasonlyPointsWon)
    {
        $this->monthlyPointsWon = $monthlyPointsWon;
        $this->virtualPoints = $virtualPoints;
        $this->seasonlyPointsWon = $seasonlyPointsWon;
    }

    /**
     * @return float
     */
    public function getSeasonlyPointsWon(): float
    {
        return $this->seasonlyPointsWon;
    }

    /**
     * @return float
     */
    public function getPointsWon(): float
    {
        return $this->monthlyPointsWon;
    }

    /**
     * @return float
     */
    public function getVirtualPoints(): float
    {
        return $this->virtualPoints;
    }
}