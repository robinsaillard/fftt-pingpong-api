<?php

require __DIR__.'/../vendor/autoload.php';
  

$env_file_path = realpath(__DIR__."/.env");
if(!is_file($env_file_path)){
    throw new ErrorException("Environment File is Missing.");
}

if(!is_readable($env_file_path)){
    throw new ErrorException("Permission Denied for reading the ".($env_file_path).".");
}

if(!is_writable($env_file_path)){
    throw new ErrorException("Permission Denied for writing on the ".($env_file_path).".");
}

$var_arrs = array();
$fopen = fopen($env_file_path, 'r');
if($fopen){
    while (($line = fgets($fopen)) !== false){
        $line_is_comment = (substr(trim($line),0 , 1) == '#') ? true: false;
        if($line_is_comment || empty(trim($line)))
            continue;
        $line_no_comment = explode("#", $line, 2)[0];
        $env_ex = preg_split('/(\s?)\=(\s?)/', $line_no_comment);
        $env_name = trim($env_ex[0]);
        $env_value = isset($env_ex[1]) ? trim($env_ex[1]) : "";
        putenv("{$env_name}={$env_value}");
    }
    fclose($fopen);
}
if (!getenv("FFTT_PASSWORD")) {
    throw new \RuntimeException('FFTT_PASSWORD environment variable is not defined. You need to define environment variables for configuration or add "symfony/dotenv" as a Composer dependency to load variables from a .env file.');
}
if (!getenv("FFTT_ID")) {
    throw new \RuntimeException('FFTT_ID environment variable is not defined. You need to define environment variables for configuration or add "symfony/dotenv" as a Composer dependency to load variables from a .env file.');
}
