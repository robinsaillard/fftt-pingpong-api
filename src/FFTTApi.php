<?php


namespace FFTTPingpongApi;

use Exception;
use Throwable;

use Accentuation\Accentuation;
use FFTTPingpongApi\Model\Club;
use FFTTPingpongApi\Model\Equipe;
use FFTTPingpongApi\Model\Joueur;
use FFTTPingpongApi\Model\Partie;
use FFTTPingpongApi\Model\Epreuve;
use FFTTPingpongApi\Service\Utils;
use FFTTPingpongApi\Model\Actualite;
use FFTTPingpongApi\Model\Organisme;
use FFTTPingpongApi\Model\Classement;
use FFTTPingpongApi\Model\Historique;
use FFTTPingpongApi\Model\ClubDetails;
use FFTTPingpongApi\Model\EquipePoule;
use FFTTPingpongApi\Model\Poule\Poule;

use FFTTPingpongApi\Model\JoueurDetails;
use FFTTPingpongApi\Model\VirtualPoints;
use GuzzleHttp\Exception\ClientException;
use FFTTPingpongApi\Service\EntityFactory;
use FFTTPingpongApi\Model\Enums\EpreuveType;
use FFTTPingpongApi\Model\Poule\PouleResult;
use FFTTPingpongApi\Model\UnvalidatedPartie;
use FFTTPingpongApi\Service\PointCalculator;
use FFTTPingpongApi\Exception\JoueurNotFound;
use FFTTPingpongApi\Model\Enums\OrganismeType;
use FFTTPingpongApi\Model\Rencontre\Rencontre;
use FFTTPingpongApi\Model\ResultIndividuel\Tour;
use FFTTPingpongApi\Model\Enums\EquipeGenderType;
use FFTTPingpongApi\Model\Enums\EquipeResultType;
use FFTTPingpongApi\Exception\InvalidCredidentials;
use FFTTPingpongApi\Exception\InvalidLienRencontre;
use FFTTPingpongApi\Exception\ClubNotFoundException;
use FFTTPingpongApi\Exception\TypeNotFoundException;
use FFTTPingpongApi\Model\Rencontre\RencontreDetails;
use FFTTPingpongApi\Exception\NoFFTTResponseException;
use FFTTPingpongApi\Exception\URIPartNotValidException;
use FFTTPingpongApi\Model\Enums\EpreuveIndividuelleType;
use FFTTPingpongApi\Exception\OrganismeNotFoundException;
use FFTTPingpongApi\Exception\InvalidURIParametersException;
use FFTTPingpongApi\Model\ResultIndividuel\PartieIndividuel;
use FFTTPingpongApi\Model\ResultIndividuel\ClassementIndividuel;

class FFTTApi
{
    private $id;
    private $password;
    private $apiRequest;

    // Jour du début du mois virtuel
    const JOURDEBUTMOISVIRTUEL = 5;

    /**
     * FFTTApi constructor
     * 
     * @param string id The "id" parameter is a string that represents the user's identification or
     * username. It is used to uniquely identify the user within the system.
     * @param string password The password parameter is a string that represents the user's password.
     */
    public function __construct(string $id, string $password)
    {
        $this->id = $id;
        $this->password = md5($password);
        $this->apiRequest = new ApiRequest($this->password, $this->id);
    }

    public function initialize()
    {
        $time = round(microtime(true) * 1000);
        $timeCrypted = hash_hmac("sha1", $time, $this->password);
        $uri = 'https://apiv2.fftt.com/mobile/pxml/xml_initialisation.php?serie=' . $this->id
            . '&tm=' . $time
            . '&tmc=' . $timeCrypted
            . '&id=' . $this->id;

        try{
            $response = $this->apiRequest->send($uri);
        }
        catch (ClientException $clientException){
            if($clientException->getResponse()->getStatusCode() === 401){
                throw new InvalidCredidentials();
            }
            throw $clientException;
        }

        return $response;
    }

    /**
     * @param string $type Organism type (F: federation, Z: Zone, L: League, D: Department)
     * @param string|null $fatherId [Optional] - Get all children of the father organism if specified 
     * @return Organisme[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     * @throws TypeNotFoundException
     */
    public function getOrganismes(string $type, ?string $fatherId = null): array
    {
        if (!OrganismeType::isExisting($type)) {
            throw new TypeNotFoundException($type);
        }

        $options = is_null($fatherId) ? ['type' => $type] : ['type' => $type, 'pere' => $fatherId];

        try {
            $organismes = $this->apiRequest->get('xml_organisme', $options)['organisme'] ?? [];
        } catch (Throwable $th) {
            throw new Exception("Aucun organisme trouvé");
        }

        $factory = new EntityFactory();
        return $factory->organismeFactory($organismes);
    }

    /**
     * The function `getEpreuves` retrieves a list of epreuves based on the organismId and
     * epreuveType provided.
     * 
     * @param string organismId The `organismId` parameter is a string that represents the ID of the
     * organism for which the epreuves are being retrieved.
     * @param string epreuveType The parameter `epreuveType` is a string that represents the type of
     * epreuve that you want to retrieve
     * @return Epreuve[] an array of Epreuve objects.
     * @throws TypeNotFoundException
     */
    public function getEpreuves(string $organismId, string $epreuveType): array
    {
        if (!EpreuveType::isExisting($epreuveType)) {
            throw new TypeNotFoundException($epreuveType);
        }

        try {
            $epreuves = $this->apiRequest->get('xml_epreuve', [
                'organisme' => $organismId,
                'type' => $epreuveType
            ])['epreuve'];
        } catch (Throwable $th) {
            throw new OrganismeNotFoundException($organismId);
        }

        $factory = new EntityFactory();
        return $factory->epreuveFactory($epreuves);

    }

    public function getDivisions(string $organismId, string $epreuveId, string $epreuveType): array
    {
        if (!EpreuveType::isExisting($epreuveType)) {
            $epreuveType = EpreuveType::getDefaultType();
        }

        $divisions = $this->apiRequest->get('xml_division', [
            'organisme' => $organismId,
            'epreuve' => $epreuveId,
            'type' => $epreuveType
        ])['division'] ?? [];

        $factory = new EntityFactory();
        return $factory->divisionFactory($divisions);
    }

    /**
     * @param string $departementId
     * @return Club[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClubsByDepartement(string $departementId): array
    {

        $data = $this->apiRequest->get('xml_club_dep2', [
            'dep' => $departementId,
        ])['club'];

        $factory = new EntityFactory();
        return $factory->clubFactory($data);
    }

    /**
     * @param string $city
     * @return Club[]
     */
    public function getClubsByCity(string $city): array
    {
        try {
            $data = $this->apiRequest->get('xml_club_b', [
                'ville' => $city,
            ])['club'];

            $factory = new EntityFactory();
            return $factory->clubFactory($data);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param string $name
     * @return Club[]
     */
    public function getClubsByName(string $name): array
    {
        return $this->getClubsByCity($name);
    }
    
    /**
     * @param string $clubNumber Club's number
     * @return Club[]
     */
    public function getClubsByNumber(string $clubNumber): array
    {
        try {
            $data = $this->apiRequest->get('xml_club_b', [
                'numero' => $clubNumber,
            ])['club'];

            $factory = new EntityFactory();
            return $factory->clubFactory($data);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param string $clubNumber Club's number
     * @return Club[]
     */
    public function getClubsByPostalCode(string $postalCode): array
    {
        try {
            $data = $this->apiRequest->get('xml_club_b', [
                'code' => $postalCode,
            ])['club'];

            $factory = new EntityFactory();
            return $factory->clubFactory($data);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param string $clubId
     * @param ?string $teamId [Optional] - get the team's hall if specified
     * @return ClubDetails
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClubDetails(string $clubId, ?string $teamId = null): ClubDetails
    {
        $options = is_null($teamId) ? ['club' => $clubId] : ['club' => $clubId, 'idequipe' => $teamId];
        $clubData = $this->apiRequest->get('xml_club_detail', $options)['club'];
        if (empty($clubData['numero'])) {
            throw new ClubNotFoundException($clubId);
        }
        $factory = new EntityFactory();
        return $factory->clubDetailsFactory($clubData);
    }

    /**
     * Renvoie une liste des joueurs provenant de la base classement
     *
     * @param string $clubNumber
     * @return Joueur[]
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     */
    public function getJoueursByClubFromClassement(string $clubNumber): array
    {
        try {
            $joueurs = $this->apiRequest->get('xml_liste_joueur', [
                    'club' => $clubNumber,
            ])['joueur'];
        } catch (NoFFTTResponseException $e) {
            throw new ClubNotFoundException($clubNumber);
        } catch (Throwable $th) {
            throw new ClubNotFoundException($clubNumber);
        }

        $factory = new EntityFactory();
        return $factory->joueurFactory($joueurs);
    }

    /**
     * Renvoie une liste des joueurs provenant de la base classement
     *
     * @param string $nom
     * @param string $prenom
     * @return Joueur[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getJoueursByNomFromClassement(string $nom, string $prenom = ""): array
    {
        $joueurs = $this->apiRequest->get('xml_liste_joueur', [
                'nom' => addslashes(Accentuation::remove($nom)),
                'prenom' => addslashes(Accentuation::remove($prenom)),
        ])['joueur'] ?? [];

        $factory = new EntityFactory();
        return $factory->joueurFactory($joueurs);
    }

    /**
     * Renvoie une liste des joueurs provenant de la base des licencies spid
     *
     * @param string $clubNumber
     * @return Joueur[]
     * @throws ClubNotFoundException
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     */
    public function getJoueursByClubFromSpid(string $clubNumber): array
    {
        try {
            $joueurs = $this->apiRequest->get('xml_liste_joueur_o', [
                    'club' => $clubNumber,
            ])['joueur'] ?? [];
        } catch (NoFFTTResponseException $e) {
            throw new ClubNotFoundException($clubNumber);
        }

        $factory = new EntityFactory();
        return $factory->joueurFactory($joueurs);
    }

    /**
     * Renvoie une liste des joueurs provenant de la base des licencies spid
     *
     * @param string $nom
     * @param string $prenom
     * @return Joueur[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getJoueursByNomFromSpid(string $nom, string $prenom = ""): array
    {
        $joueurs = $this->apiRequest->get('xml_liste_joueur_o', [
                'nom' => addslashes(Accentuation::remove($nom)),
                'prenom' => addslashes(Accentuation::remove($prenom)),
        ])['joueur'] ?? [];

        $factory = new EntityFactory();
        return $factory->joueurFactory($joueurs);
    }

    /**
     * Renvoie une liste des joueurs provenant de la base des licencies spid
     *
     * @param string $nom
     * @param string $prenom
     * @return Joueur[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getJoueursByLicenceFromSpid(string $licenceNumber): array
    {
        $joueurs = $this->apiRequest->get('xml_liste_joueur_o', [
                'licence' => $licenceNumber
            ]
        )['joueur'] ?? [];

        $factory = new EntityFactory();
        return $factory->joueurFactory($joueurs);
    }

    /**
     * @param string $licenceNumber
     * @return JoueurDetails|null
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getJoueurDetailsByLicence(string $licenceNumber): ?JoueurDetails
    {
        try {
            $data = $this->apiRequest->get('xml_licence_b', [
                    'licence' => $licenceNumber,
                ]
            )['licence'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceNumber);
        }

        $factory = new EntityFactory();
        return $factory->joueurDetailsFactory($data)[0] ?? null;
    }

    /**
     * @param string $licenceNumber
     * @return JoueurDetails[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getJoueurDetailsByClub(string $clubNumber): array
    {
        $data = $this->apiRequest->get('xml_licence_b', [
                'club' => $clubNumber,
            ]
        )['licence'] ?? [];

        $factory = new EntityFactory();
        return $factory->joueurDetailsFactory($data, $clubNumber);
    }

    /**
     * @param string $licenceNumber
     * @return Classement
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getClassementJoueurByLicence(string $licenceNumber): Classement
    {
        try {
            $joueurDetails = $this->apiRequest->get('xml_joueur', [
                'licence' => $licenceNumber,
            ])['joueur'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceNumber);
        }

        $factory = new EntityFactory();
        return $factory->classementFactory($joueurDetails);
    }

    /**
     * @param string $licenceNumber
     * @return Historique[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws JoueurNotFound
     */
    public function getHistoriqueJoueurByLicence(string $licenceNumber): array
    {
        try {
            $classements = $this->apiRequest->get('xml_histo_classement', [
                'numlic' => $licenceNumber,
            ])['histo'];
        } catch (NoFFTTResponseException $e) {
            throw new JoueurNotFound($licenceNumber);
        }
        $factory = new EntityFactory();
        return $factory->historiqueFactory($classements);
    }

    /**
     * @param string $licenceNumber
     * @return Partie[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     */
    public function getPartiesJoueurByLicence(string $licenceNumber): array
    {
        try {
            $parties = $this->apiRequest->get('xml_partie_mysql', [
                'licence' => $licenceNumber,
            ])['partie'] ?? [];
        } catch (NoFFTTResponseException $e) {
            return [];
        }
        
        $factory = new EntityFactory();
        return $factory->partieFactory($parties);
    }

    /**
     * @param string $licenceNumber
     * @return UnvalidatedPartie[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     */
    public function getUnvalidatedPartiesJoueurByLicence(string $licenceNumber): array
    {
        $validatedParties = $this->getPartiesJoueurByLicence($licenceNumber);

        try {
            $allParties = $this->apiRequest->get('xml_partie', [
                    'numlic' => $licenceNumber,
                ])["partie"] ?? [];
        } catch (NoFFTTResponseException $e) {
            $allParties = [];
        }

        $factory = new EntityFactory();
        return $factory->UnvalidatedPartieFactory($allParties, $validatedParties);
    }

    /**
     * @param string $licenceNumber
     * @return VirtualPoints Objet contenant les points gagnés/perdus et le classement virtuel du joueur
     */
    public function getJoueurVirtualPoints(string $licenceNumber): VirtualPoints
    {
        $pointCalculator = new PointCalculator();

        try {
            $classement = $this->getClassementJoueurByLicence($licenceNumber);
            $virtualMonthlyPointsWon = 0.0;
            $virtualMonthlyPoints = 0.0;
            $latestMonth = null;
            $monthPoints = round($classement->getPoints(), 1);

            $unvalidatedParties = $this->getUnvalidatedPartiesJoueurByLicence($licenceNumber);

            usort($unvalidatedParties, function (UnvalidatedPartie $a, UnvalidatedPartie $b) {
                return $a->getDate() >= $b->getDate();
            });

            foreach ($unvalidatedParties as $unvalidatedParty) {
                if (!$latestMonth) {
                    $latestMonth = $unvalidatedParty->getDate()->format("m");
                } else {
                    if ($latestMonth != $unvalidatedParty->getDate()->format("m") && $unvalidatedParty->getDate()->format("j") > self::JOURDEBUTMOISVIRTUEL - 1) {
                        $monthPoints = round($classement->getPoints() + $virtualMonthlyPointsWon, 1);
                        $latestMonth = $unvalidatedParty->getDate()->format("m");
                    }
                }

                $coeff = $unvalidatedParty->getCoefficientChampionnat();

                if (!$unvalidatedParty->isForfait()) {
                    $adversairePoints = $unvalidatedParty->getAdversaireClassement();

                    /**
                     * TODO Refactoring in method
                     */

                    try {
                        $availableJoueurs = $this->getJoueursByNomFromClassement($unvalidatedParty->getAdversaireNom(), $unvalidatedParty->getAdversairePrenom());
                        foreach ($availableJoueurs as $availableJoueur) {
                            if (round(($unvalidatedParty->getAdversaireClassement() / 100)) == $availableJoueur->getPoints()) {
                                $classementJoueur = $this->getClassementJoueurByLicence($availableJoueur->getLicence());
                                $adversairePoints = round($classementJoueur->getPoints(), 1);
                                break;
                            }
                        }

                    } catch (NoFFTTResponseException $e) {
                        $adversairePoints = $unvalidatedParty->getAdversaireClassement();
                    }catch (InvalidURIParametersException $e) {
                        $adversairePoints = $unvalidatedParty->getAdversaireClassement();
                    }

                    $points = $unvalidatedParty->isVictoire()
                        ? $pointCalculator->getPointVictory($monthPoints, floatval($adversairePoints))
                        : $pointCalculator->getPointDefeat($monthPoints, floatval($adversairePoints));
                    $virtualMonthlyPointsWon += $points * $coeff;
                }
            }

            $virtualMonthlyPoints = $monthPoints + $virtualMonthlyPointsWon;
            return new VirtualPoints(
                $virtualMonthlyPointsWon,
                $virtualMonthlyPoints,
                $virtualMonthlyPoints - $classement->getPointsInitials()
            );
        } catch (JoueurNotFound $e) {
            return new VirtualPoints(0.0, 0.0, 0.0);
        }
    }

    /**
     * Renvoie une liste des équipes d’un club
     *
     * @param string $clubNumber
     * @param string|null $type
     * @return Equipe[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     * @throws TypeNotFoundException
     */
    public function getEquipesByClub(string $clubNumber, string $type = null)
    {
        $params = ['numclu' => $clubNumber];
        if ($type) {
            if (!EquipeGenderType::isExisting($type))
                throw new TypeNotFoundException($type);
            $params['type'] = $type;
        }
        $data = $this->apiRequest->get('xml_equipe', $params
        )['equipe'] ?? [];

        $factory = new EntityFactory();
        return $factory->equipeFactory($data);
    }

    /**
     * @param string $epreuveId
     * @param string $divisionId
     * @return Tour[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelTour(string $epreuveId, string $divisionId)
    {        
        $tours = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::POULE->value,
            'epr' => $epreuveId,
            'res_division' => $divisionId
        ])["tour"] ?? [];

        $factory = new EntityFactory();
        return $factory->TourFactory($tours);
    }

    /**
     * @param string $lien
     * @return Tour[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelTourByLien(string $lien)
    {        
        $tours = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::POULE->value,
        ], $lien)["tour"] ?? [];

        $factory = new EntityFactory();
        return $factory->TourFactory($tours);
    }

    /**
     * @param string $epreuveId
     * @param string $divisionId
     * @return ClassementIndividuel[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelClassement(string $epreuveId, string $divisionId)
    {        
        $classements = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::CLASSEMENT->value,
            'epr' => $epreuveId,
            'res_division' => $divisionId
        ])["classement"] ?? [];

        $factory = new EntityFactory();
        return $factory->classementIndividuelFactory($classements);
    }

    /**
     * @param string $lien
     * @return ClassementIndividuel[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelClassementByLien(string $lien)
    {        
        $classements = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::CLASSEMENT->value
        ], $lien)["classement"] ?? [];

        $factory = new EntityFactory();
        return $factory->classementIndividuelFactory($classements);
    }

    /**
     * @param string $epreuveId
     * @param string $divisionId
     * @return PartieIndividuel[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelParties(string $epreuveId, string $divisionId)
    {        
        $parties = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::PARTIE->value,
            'epr' => $epreuveId,
            'res_division' => $divisionId
        ])["partie"] ?? [];

        $factory = new EntityFactory();
        return $factory->partieIndividuelFactory($parties);
    }

    /**
     * @param string $lien
     * @return PartieIndividuel[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultIndividuelPartiesByLien(string $lien)
    {        
        $parties = $this->apiRequest->get('xml_result_indiv', [
            'action' => EpreuveIndividuelleType::PARTIE->value,
        ], $lien)["partie"] ?? [];

        $factory = new EntityFactory();
        return $factory->partieIndividuelFactory($parties);
    }

    /**
     * Renvoie le classement général d’une division du critérium « nouvelle formule »
     *
     * @param string $divisionId
     * @return PartieIndividuel[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClassementCriterium(string $divisionId)
    {        
        $classements = $this->apiRequest->get('xml_res_cla', [
            'res_division' => $divisionId
        ])["classement"] ?? [];

        $factory = new EntityFactory();
        return $factory->classementIndividuelFactory($classements);
    }

    /**
     * @param string $lien
     * @return EquipePoule[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getClassementPouleByLienDivision(string $lien): array
    {
        $data = $this->apiRequest->get('xml_result_equ',[
            "action" => EquipeResultType::CLASSEMENT->value
        ], $lien)['classement'] ?? [];

        $factory = new EntityFactory();
        return $factory->equipePouleFactory($data);
    }

    /**
     * @param string $lien
     * @return Rencontre[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getRencontrePouleByLienDivision(string $lien): array
    {
        $data = $this->apiRequest->get('xml_result_equ', ["action" => EquipeResultType::RENCONTRES->value], $lien)['tour'];
        if (empty($data)) {
            $data = [];
        }

        $factory = new EntityFactory();
        return $factory->rencontreFactory($data);
    }

    /**
     * @param string $lien
     * @return Poule[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getPoulesEquipeByLien(string $lien, EquipeResultType $action = EquipeResultType::POULE): array
    {
        $data = $this->apiRequest->get('xml_result_equ', ["action" => EquipeResultType::POULE], $lien)[$action] ?? [];

        $factory = new EntityFactory();
        return $factory->pouleFactory($data);
    }

    /**
     * @param string $lien
     * @return Poule[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getPoulesClubsByLien(string $lien): array //todo Voir quand api remarchera
    {
        $data = $this->apiRequest->get('xml_result_equ', ["action" => EquipeResultType::INITIAL->value], $lien)['tour'] ?? [];
        $factory = new EntityFactory();
        return $factory->pouleFactory($data);
    }


    /**
     * @param string $lien
     * @return PouleClassement[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getPoulesClassementByLien(string $lien): array
    {
        try {
            // Essai d'appel API
            $data = $this->apiRequest->get('xml_result_equ', ["action" => EquipeResultType::INITIAL->value], $lien)['classement'] ?? [];
            $factory = new EntityFactory();
            return $factory->pouleClassementFactory($data);
        } catch (URIPartNotValidException | InvalidURIParametersException | NoFFTTResponseException $e) {
            return ['error' => $e->getMessage()];
        } catch (\Exception $e) {
            return ['error' => 'Erreur inattendue lors de la récupération des données'];
        }
    }


    /**
     * @param Equipe $equipe
     * @return Rencontre[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getProchainesRencontresEquipe(string $equipeName, string $lienDivision): array
    {
        $explode = explode(" - ", $equipeName);
        if(count($explode) === 2){
            $equipeName = $explode[0];
        }

        $rencontres = $this->getRencontrePouleByLienDivision($lienDivision);

        $prochainesRencontres = [];
        foreach ($rencontres as $rencontre) {
            if ($rencontre->getDateReelle() === null && $rencontre->getNomEquipeA() === $equipeName || $rencontre->getNomEquipeB() === $equipeName) {
                $prochainesRencontres[] = $rencontre;
            }
        }
        return $prochainesRencontres;
    }

    /**
     * Renvoie les résultats d’une poule ou plusieurs poules de championnat par équipes
     *
     * @param string[] $pouleIds
     * @return PouleResult[]
     * @throws InvalidURIParametersException
     * @throws URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getResultsByPoules(array $pouleIds): array
    {
        $data = $this->apiRequest->get('xml_rencontre_equ', ["poule" => implode('|', $pouleIds)])['tour'] ?? [];

        $factory = new EntityFactory();
        return $factory->pouleResultFactory($data);
    }

    /**
     * Renvoie les informations détaillées d’une rencontre
     *
     * @param string $lienRencontre
     * @param string $clubEquipeA
     * @param string $clubEquipeB
     * @return RencontreDetails
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws InvalidLienRencontre
     * @throws NoFFTTResponseException
     */
    public function getDetailsRencontreByLien(string $lienRencontre, string $clubEquipeA = "", string $clubEquipeB = ""): RencontreDetails
    {
        $data = $this->apiRequest->get('xml_chp_renc', [], $lienRencontre);
        if (!(isset($data['resultat']) && isset($data['joueur']) && isset($data['partie']))) {
            throw new InvalidLienRencontre($lienRencontre);
        }
        $factory = new EntityFactory($this);
        return $factory->rencontreDetailsFactory($data, $clubEquipeA, $clubEquipeB);
    }

    /**
     * @return Actualite[]
     * @throws Exception\InvalidURIParametersException
     * @throws Exception\URIPartNotValidException
     * @throws NoFFTTResponseException
     */
    public function getActualites(): array
    {
        $data = $this->apiRequest->get('xml_new_actu')['news'] ?? [];

        $factory = new EntityFactory();
        return $factory->actualiteFactory($data);
    }
}
