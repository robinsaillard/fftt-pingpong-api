<?php


namespace FFTTPingpongApi\Model\Rencontre;


class RencontreDetails
{
    /**
     * @var string $nomEquipeA
     */
    private $nomEquipeA;

    /**
     * @var string $nomEquipeB
     */
    private $nomEquipeB;

    /**
     * @var int $scoreEquipeA
     */
    private $scoreEquipeA;

    /**
     * @var int $scoreEquipeB
     */
    private $scoreEquipeB;

    /**
     * @var Joueur[] $joueursA
     */
    private $joueursA;

    /**
     * @var Joueur[] $joueursB
     */
    private $joueursB;

    /**
     * @var Partie[] $parties
     */
    private $parties;

    /**
     * @var float $expectedScoreEquipeA
     */
    private $expectedScoreEquipeA;

    /**
     * @var float $expectedScoreEquipeB
     */
    private $expectedScoreEquipeB;

    /**
     * RencontreDetails constructor
     * 
     * @param string $nomEquipeA The name of team A. It is a string.
     * @param string $nomEquipeB The name of the second team in the match.
     * @param int $scoreEquipeA The score of team A in a game or match.
     * @param int $scoreEquipeB The score of team B in a game or match.
     * @param array $joueursA An array containing the players of team A.
     * @param array $joueursB An array containing the players of team B.
     * @param array $parties The "parties" parameter is an array that represents the different games or
     * matches played between the two teams. Each element in the array represents a single game and can
     * contain any relevant information about that game, such as the date, location, or any other
     * details.
     * @param float $expectedScoreEquipeA The expected score for team A in a game or match.
     * @param float $expectedScoreEquipeB The expected score for team B in a game or match.
     */
    public function __construct(
        string $nomEquipeA,
        string $nomEquipeB,
        int $scoreEquipeA,
        int $scoreEquipeB,
        array $joueursA,
        array $joueursB,
        array $parties,
        float $expectedScoreEquipeA,
        float $expectedScoreEquipeB
    )
    {
        $this->nomEquipeA = $nomEquipeA;
        $this->nomEquipeB = $nomEquipeB;
        $this->scoreEquipeA = $scoreEquipeA;
        $this->scoreEquipeB = $scoreEquipeB;
        $this->joueursA = $joueursA;
        $this->joueursB = $joueursB;
        $this->parties = $parties;
        $this->expectedScoreEquipeA = $expectedScoreEquipeA;
        $this->expectedScoreEquipeB = $expectedScoreEquipeB;
    }

    /**
     * @return string
     */
    public function getNomEquipeA(): string
    {
        return $this->nomEquipeA;
    }

    /**
     * @return string
     */
    public function getNomEquipeB(): string
    {
        return $this->nomEquipeB;
    }

    /**
     * @return int
     */
    public function getScoreEquipeA(): int
    {
        return $this->scoreEquipeA;
    }

    /**
     * @return int
     */
    public function getScoreEquipeB(): int
    {
        return $this->scoreEquipeB;
    }

    /**
     * @return Joueur[]
     */
    public function getJoueursA(): array
    {
        return $this->joueursA;
    }

    /**
     * @return Joueur[]
     */
    public function getJoueursB(): array
    {
        return $this->joueursB;
    }

    /**
     * @return Partie[]
     */
    public function getParties(): array
    {
        return $this->parties;
    }

    /**
     * @return float
     */
    public function getExpectedScoreEquipeA(): float
    {
        return $this->expectedScoreEquipeA;
    }

    /**
     * @return float
     */
    public function getExpectedScoreEquipeB(): float
    {
        return $this->expectedScoreEquipeB;
    }
}