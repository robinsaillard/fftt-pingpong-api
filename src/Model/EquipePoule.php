<?php


namespace FFTTPingpongApi\Model;


class EquipePoule
{
    /**
     * @var string pouleName
     */
    private $pouleName;

    /**
     * @var int classement
     */
    private $classement;

    /**
     * @var string nomEquipe
     */
    private $nomEquipe;

    /**
     * @var int matchJouees
     */
    private $matchJouees;

    /**
     * @var int points
     */
    private $points;

    /**
     * @var string numero
     */
    private $numero;

    /**
     * @var int victoires
     */
    private $victoires;

    /**
     * @var int defaites
     */
    private $defaites;

    /**
     * @var int idEquipe
     */
    private $idEquipe;

    /**
     * @var string idClub
     */
    private $idClub;

    /**
     * @var int nuls
     */
    private $nuls;

    /**
     * @var int penaliteForfait
     */
    private $penaliteForfait;

    /**
     * @var int totalPartieVictoire
     */
    private $totalPartieVictoire;

    /**
     * @var int totalPartieDefaites
     */
    private $totalPartieDefaites;

    /**
     * EquipePoule constructor
     * 
     * @param string pouleName The name of the group or division in which the team belongs.
     * @param int classement The parameter "classement" represents the ranking or position of the team
     * in a league or tournament. It is an integer value.
     * @param string nomEquipe The parameter "nomEquipe" is a string that represents the name of the
     * team.
     * @param int matchJouees The parameter "matchJouees" represents the number of matches played by
     * the team.
     * @param int points The "points" parameter represents the total number of points earned by the
     * team in a competition. Points are typically awarded for winning, drawing, or losing a match.
     * @param string numero The "numero" parameter is a string that represents the team's number or
     * identifier.
     * @param int victoires The parameter "victoires" represents the number of victories for a team.
     * @param int defaites The parameter "defaites" represents the number of defeats a team has in a
     * competition.
     * @param int idEquipe The idEquipe parameter is an integer that represents the unique identifier
     * of the team.
     * @param string idClub The parameter "idClub" is a string that represents the ID of the club
     * associated with the team.
     * @param int nuls The parameter "nuls" represents the number of matches that ended in a draw for a
     * team.
     * @param int penaliteForfait The parameter "penaliteForfait" represents the number of penalty
     * points or penalties for forfeiting a match.
     * @param int totalPartieVictoire The parameter "totalPartieVictoire" represents the total number
     * of games won by the team.
     * @param int totalPartieDefaites The parameter "totalPartieDefaites" represents the total number
     * of games lost by the team.
     */
    public function __construct(
        string $pouleName,
        int $classement,
        string $nomEquipe,
        int $matchJouees,
        int $points,
        string $numero,
        int $victoires,
        int $defaites,
        int $idEquipe,
        string $idClub,
        int $nuls,
        int $penaliteForfait,
        int $totalPartieVictoire,
        int $totalPartieDefaites
    )
    {
        $this->pouleName = $pouleName;
        $this->classement = $classement;
        $this->nomEquipe = $nomEquipe;
        $this->matchJouees = $matchJouees;
        $this->points = $points;
        $this->numero = $numero;
        $this->victoires = $victoires;
        $this->defaites = $defaites;
        $this->idEquipe = $idEquipe;
        $this->idClub = $idClub;
        $this->nuls = $nuls;
        $this->penaliteForfait = $penaliteForfait;
        $this->totalPartieVictoire = $totalPartieVictoire;
        $this->totalPartieDefaites = $totalPartieDefaites;
    }

    /**
     * @return string
     */
    public function getPouleName(): string
    {
        return $this->pouleName;
    }

    /**
     * @return int
     */
    public function getClassement(): int
    {
        return $this->classement;
    }

    /**
     * @return string
     */
    public function getNomEquipe(): string
    {
        return $this->nomEquipe;
    }

    /**
     * @return int
     */
    public function getMatchJouees(): int
    {
        return $this->matchJouees;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @return string
     */
    public function getNumero(): string
    {
        return $this->numero;
    }

    /**
     * @return int
     */
    public function getVictoires(): int
    {
        return $this->victoires;
    }

    /**
     * @return int
     */
    public function getDefaites(): int
    {
        return $this->defaites;
    }

    /**
     * @return int
     */
    public function getIdEquipe(): int
    {
        return $this->idEquipe;
    }

    /**
     * @return string
     */
    public function getIdClub(): string
    {
        return $this->idClub;
    }

    /**
     * @return int
     */
    public function getNuls(): int
    {
        return $this->nuls;
    }

    /**
     * @return int
     */
    public function getPenaliteForfait(): int
    {
        return $this->penaliteForfait;
    }

    /**
     * @return int
     */
    public function getTotalPartieVictoire(): int
    {
        return $this->totalPartieVictoire;
    }

    /**
     * @return int
     */
    public function getTotalPartieDefaite(): int
    {
        return $this->totalPartieDefaites;
    }
}