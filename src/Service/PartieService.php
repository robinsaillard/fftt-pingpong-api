<?php

namespace FFTTPingpongApi\Service;

use FFTTPingpongApi\Model\Rencontre\Partie;

class PartieService
{
    /**
     * @param Partie[] $parties
     * @param array<string, Joueur> $joueursAFormatted
     * @param array<string, Joueur> $joueursBFormatted
     * @return array{expectedA: float, expectedB: float}
     */
    public function getExpectedPoints(array $parties, array $joueursAFormatted, array $joueursBFormatted): array
    {
        $expectedA = 0;
        $expectedB = 0;

        foreach ($parties as $partie) {
            $adversaireA = $partie->getAdversaireA();
            $adversaireB = $partie->getAdversaireB();

            if (isset($joueursAFormatted[$adversaireA])) {
                $joueurA = $joueursAFormatted[$adversaireA];
                $joueurAPoints = $joueurA->getPoints();
            } else {
                $joueurAPoints = 'NONE';
            }

            if (isset($joueursBFormatted[$adversaireB])) {
                $joueurB = $joueursBFormatted[$adversaireB];
                $joueurBPoints = $joueurB->getPoints();
            } else {
                $joueurBPoints = 'NONE';
            }

            if ($joueurAPoints === $joueurBPoints) {
                $expectedA += 0.5;
                $expectedB += 0.5;
            } elseif ($joueurAPoints > $joueurBPoints) {
                $expectedA += 1;
            } else {
                $expectedB += 1;
            }
        }

        return [
            'expectedA' => $expectedA,
            'expectedB' => $expectedB,
        ];
    }

    /**
     * @param Partie[] $parties
     * @return array{scoreA: int, scoreB: int}
     */
    public function getScores(array $parties): array
    {
        $scoreA = 0;
        $scoreB = 0;

        foreach ($parties as $partie) {
            $scoreA += $partie->getScoreA();
            $scoreB += $partie->getScoreB();
        }

        return [
            'scoreA' => $scoreA,
            'scoreB' => $scoreB,
        ];
    }

    /**
     * @param array $data
     * @return Partie[]
     */
    public function getParties(array $data): array
    {
        $parties = [];
        foreach ($data as $partieData) {
            if(!is_array($partieData['detail'])) {
                $setDetails = explode(" ", $partieData['detail']);
            } else {
                $setDetails = [];
            }

            $parties[] = new Partie(
                $partieData['ja'] === [] ? 'Absent Absent' : $partieData['ja'],
                $partieData['jb'] === [] ? 'Absent Absent' : $partieData['jb'],
                $partieData['scorea'] === '-' ? 0 : intval($partieData['scorea']),
                $partieData['scoreb'] === '-' ? 0 : intval($partieData['scoreb']),
                $setDetails
            );
        }
        return $parties;
    }
}