<?php

namespace FFTTPingpongApi\Model\Enums;

enum EpreuveType: string implements EnumInterface
{
    case EQUIPE = 'E';
    case INDIVIDUELLE = 'I';

    /**
     * @return string
     */
    public static function getDefaultType(): string
    {
        return self::EQUIPE->value;
    }

    /**
     * Check if type exists is enum
     *
     * @param string $type
     * @return bool
     */
    public static function isExisting(string $type): bool
    {
        return AbstractEnum::isExisting(self::cases(), $type);
    }
}
?>
