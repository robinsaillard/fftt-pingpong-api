<?php


namespace FFTTPingpongApi\Service;


use FFTTPingpongApi\Model\Equipe;

class Utils
{
    public static function returnNomPrenom(string $s) {
        $nom = [];
        $prenom = [];
        $words = explode(" ", $s);

        foreach ($words as $word){
            $lastChar = substr($word, -1);
            mb_strtolower($lastChar, "UTF-8") == $lastChar ? $prenom[] = $word : $nom[] = $word;
        }

        return [
            implode(" ", $nom),
            implode(" ", $prenom),
        ];
    }

    public static function formatPoints(string $classement) : string {
        $explode = explode("-", $classement);
        if(count($explode) == 2){
            $classement=$explode[1];
        }
        return $classement;
    }

    public static function extractNomEquipe(Equipe $equipe): string{
        $explode = explode(" - ", $equipe->getLibelle());
        if(count($explode) === 2){
            return $explode[0];
        }
        return $equipe->getLibelle();
    }

    public static function extractClub(Equipe $equipe): string{
        $nomEquipe = self::extractNomEquipe($equipe);
        return preg_replace('/ [0-9]+$/', '', $nomEquipe);
    }

    public static function removeAccentLowerCaseRegex(string $string): string {
        return str_replace('?', '.', mb_convert_case(\Transliterator::create('NFD; [:Nonspacing Mark:] Remove;')
            ->transliterate($string), MB_CASE_LOWER, "UTF-8"));
    }

    public static function wrappedArrayIfUnique($array): array
    {
        return count($array) == count($array, COUNT_RECURSIVE) ? [$array] : $array;
    }
}