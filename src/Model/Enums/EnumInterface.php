<?php

namespace FFTTPingpongApi\Model\Enums;

interface EnumInterface
{
    public static function getDefaultType(): string;

    public static function isExisting(string $searchValue): bool;

}