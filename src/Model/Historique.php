<?php


namespace FFTTPingpongApi\Model;


class Historique
{
    /**
     * @var string|null $anneeDebut
     */
    private $place;

    /**
     * @var int $anneeDebut
     */
    private $anneeDebut;

    /**
     * @var int $anneeFin
     */
    private $anneeFin;

    /**
     * @var int $phase
     */
    private $phase;

    /**
     * @var int $points
     */
    private $points;

    /**
     * Historique constructor
     * 
     * @param string|null $place
     * @param int $anneeDebut The starting year of the event or phase.
     * @param int $anneeFin The ending year of the event or phase.
     * @param int $phase The "phase" parameter represents the phase of a certain event or process. It is
     * an integer value that indicates the current phase of the event or process.
     * @param int $points The "points" parameter is an integer that represents the number of points. It
     * is used to store the points earned in a particular phase or event.
     */
    public function __construct(?string $place, int $anneeDebut, int $anneeFin, int $phase, int $points)
    {
        $this->place = $place;
        $this->anneeDebut = $anneeDebut;
        $this->anneeFin = $anneeFin;
        $this->phase = $phase;
        $this->points = $points;
    }

    /**
     * @return string|null
     */
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * @return int
     */
    public function getAnneeDebut(): int
    {
        return $this->anneeDebut;
    }

    /**
     * @return int
     */
    public function getAnneeFin(): int
    {
        return $this->anneeFin;
    }

    /**
     * @return int
     */
    public function getPhase(): int
    {
        return $this->phase;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

}