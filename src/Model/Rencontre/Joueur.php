<?php

namespace FFTTPingpongApi\Model\Rencontre;

class Joueur
{
    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var string $prenom
     */
    private $prenom;

    /**
     * @var int|null $points
     */
    private $points;

    /**
     * @var int|null $sexe
     */
    private $sexe;

    /**
     * @var string $licence
     */
    private $licence;

    /**
     * Joueur constructor
     * 
     * @param string $nom The "nom" parameter is a string that represents the last name of a person.
     * @param string $prenom The parameter "prenom" is a string that represents the first name of a
     * person.
     * @param string $licence The "licence" parameter is a string that represents the license of a
     * person. It could be a driver's license, professional license, or any other type of license.
     * @param int|null points $The "points" parameter is an optional integer that represents the number of points
     * the person has. It can be null if the person doesn't have any points.
     * @param string|null sexe $The "sexe" parameter is a nullable string that represents the gender of a person.
     */
    public function __construct(string $nom, string $prenom,  string $licence, ?int $points, ?string $sexe)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->licence = $licence;
        $this->points = $points;
        $this->sexe = $sexe;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return string
     */
    public function getLicence(): string
    {
        return $this->licence;
    }

    /**
     * @return int|null
     */
    public function getPoints(): ?int
    {
        return $this->points;
    }

    /**
     * @return string|null
     */
    public function getSexe(): ?string
    {
        return $this->sexe;
    }
}