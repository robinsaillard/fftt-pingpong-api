<?php

namespace FFTTPingpongApi\Model\Enums;

enum EpreuveIndividuelleType: string implements EnumInterface
{
    case POULE = 'poule';
    case CLASSEMENT = 'classement';
    case PARTIE = 'partie';

    /**
     * @return string
     */
    public static function getDefaultType(): string
    {
        return self::CLASSEMENT->value;
    }

    /**
     * Check if type exists is enum
     *
     * @param string $type
     * @return bool
     */
    public static function isExisting(string $type): bool
    {
        return AbstractEnum::isExisting(self::cases(), $type);
    }
}
?>
