<?php


namespace FFTTPingpongApi\Model;


class Joueur
{
    /**
     * @var string $licence
     */
    private $licence;
    
    /**
     * @var string|null $clubId
     */
    private $clubId;
    
    /**
     * @var string|null $club
     */
    private $club;
    
    /**
     * @var string $nom
     */
    private $nom;
    
    /**
     * @var string $prenom
     */
    private $prenom;

    /**
     * @var string|null Points du joueur ou classement si classé dans les 1000 premiers français
     */
    private $points;

    /**
     * Joueur constructor
     * 
     * @param string $licence The licence parameter is a string that represents the license of a person.
     * It is used to identify and track the person's membership or participation in a club or
     * organization.
     * @param string|null $clubId The clubId parameter is a string that represents the ID of the club.
     * @param string|null $club The "club" parameter is a string that represents the name of the club to
     * which the person belongs.
     * @param string $nom The "nom" parameter is a string that represents the last name of a
     * person.
     * @param string $prenom The parameter "prenom" represents the first name of a person.
     * @param string|null $points The "points" parameter is a nullable string that represents the points of the
     * object. It can be either a string value or null.
     */
    public function __construct(string $licence, ?string $clubId, ?string $club, string $nom, string $prenom, ?string $points)
    {
        $this->licence = $licence;
        $this->clubId = $clubId;
        $this->club = $club;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->points = $points;
    }

    /**
     * @return string
     */
    public function getLicence(): string
    {
        return $this->licence;
    }

    /**
     * @return string|null
     */
    public function getClubId(): string
    {
        return $this->clubId;
    }

    /**
     * @return string|null
     */
    public function getClub(): string
    {
        return $this->club;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @return string
     */
    public function getPoints(): ?string
    {
        return $this->points;
    }
}