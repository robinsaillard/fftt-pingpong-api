<?php


namespace FFTTPingpongApi\Model;


class Equipe
{
    /**
     * @var string libelle
     */
    private $libelle;

    /**
     * @var string division
     */
    private $division;

    /**
     * @var string|null lienDivision
     */
    private $lienDivision;

    /**
     * @var string epreuveId
     */
    private $epreuveId;

    /**
     * @var string epreuveLibelle
     */
    private $epreuveLibelle;

    /**
     * Equipe constructor
     * 
     * @param string $libelle The "libelle" parameter is a string that represents the label or name of
     * something.
     * @param string $division The division parameter is a string that represents the division of the
     * object. It could be a category or a group that the object belongs to.
     * @param string|null $lienDivision The parameter "lienDivision" is a string that represents the link to
     * the division.
     * @param string $epreuveId The epreuveId parameter is a string that represents the ID of an epreuve
     * (test or exam).
     * @param string $epreuveLibelle The parameter `epreuveLibelle` is a string that represents the
     * label or name of an epreuve.
     */
    public function __construct(string $libelle, string $division, ?string $lienDivision, string $epreuveId, string $epreuveLibelle)
    {
        $this->libelle = $libelle;
        $this->division = $division;
        $this->lienDivision = $lienDivision;
        $this->epreuveId = $epreuveId;
        $this->epreuveLibelle = $epreuveLibelle;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getDivision(): string
    {
        return $this->division;
    }

    /**
     * @return string|null
     */
    public function getLienDivision(): ?string
    {
        return $this->lienDivision;
    }

    /**
     * @return string
     */
    public function getEpreuveId(): string
    {
        return $this->epreuveId;
    }

    /**
     * @return string
     */
    public function getEpreuveLibelle(): string
    {
        return $this->epreuveLibelle;
    }
}