<?php


namespace FFTTPingpongApi\Model\Poule;

class Poule
{
    /**
     * @var string $url
     */
    private $url;

    /**
     * @var string $libelle
     */
    private $libelle;
    
    /**
     * Poule constructor
     * 
     * @param string $url A string containing the poule ID and the division ID.
     * @param string $libelle The parameter "libelle" is a string that represents the label or name of
     * the poule.
     */
    public function __construct(string $url, string $libelle)
    {
        $this->url = $url;
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }
}