<?php

namespace FFTTPingpongApi\Service;

use DateTime;


class RencontreService
{
    const JOURDEBUTMOISVIRTUEL = 5;
    
    /**
     * Détermine si la date d'une rencontre passée en paramètre correspond à la saison en cours
     * @param DateTime $dateRencontre
     * @return bool
     */
    public function isInCurrentSaison(DateTime $dateRencontre): bool {
        $today = new DateTime();

        $actualDay = $today->format('j');
        $actualMonth = $today->format('n');
        $actualYear = $today->format('Y');

        $dateDebutSaison = new DateTime($actualYear + ($actualMonth >= 7 && $actualDay >= 1 ? 0 : -1 ) . '-07-01');
        $dateFinSaison = new DateTime($actualYear + ($actualMonth >= 7 && $actualDay >= 1 ? 1 : 0 ) . '-07-01');

        return $dateRencontre >= $dateDebutSaison && $dateRencontre <= $dateFinSaison;
    }

    /**
     * Détermine si la date d'une rencontre passée en paramètre correspond au mois virtuel en cours, calculée du 1er [mois] inclus au 4 [mois+1] inclus
     * En d'autres termes, les rencontres virtuelles sont prises à partir du 1er du mois précédent jusqu'au 4 du mois en cours
     * Exemple : si nous sommes le 6 Octobre, nous prenons les rencontre du 1er Septembre à aujourd'hui.
     * Exemple : si nous sommes le 15 Octobre, nous prenons les rencontre du 1er Octobre (les points virtuels sont connus à partir du 5 et les rencontres sont comptabilisées du 1er au 31 du mois) à aujourd'hui.
     * @param DateTime $dateRencontre
     * @return bool
     */
    public function isInCurrentVirtualMonth(DateTime $dateRencontre): bool {
        $today = new DateTime();

        $actualMonth = $today->format('n');
        $actualYear = $today->format('Y');

        if ($today->format('j') > self::JOURDEBUTMOISVIRTUEL - 1){
            $debutMoisVirtuel = $actualMonth;
        } else {
            if ($actualMonth > 1) {
                $debutMoisVirtuel = $actualMonth - 1;
            }
            else {
                $debutMoisVirtuel = 12;
                $actualYear -= 1;
            }
        }

        $dateDebutMoisVirtuel = new DateTime($actualYear . '-' . $debutMoisVirtuel . '-01');
        $dateFinMoisVirtuel = new DateTime();

        return $dateRencontre >= $dateDebutMoisVirtuel && $dateRencontre <= $dateFinMoisVirtuel;
    }
}