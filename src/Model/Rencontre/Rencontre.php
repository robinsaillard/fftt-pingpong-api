<?php


namespace FFTTPingpongApi\Model\Rencontre;


class Rencontre
{
    /**
     * @var string $libelle
     */
    private $libelle;

    /**
     * @var string $nomEquipeA
     */
    private $nomEquipeA;

    /**
     * @var string $nomEquipeB
     */
    private $nomEquipeB;

    /**
     * @var int $scoreEquipeA
     */
    private $scoreEquipeA;

    /**
     * @var int $scoreEquipeB
     */
    private $scoreEquipeB;

    /**
     * @var string $lien
     */
    private $lien;

    /**
     * @var \DateTime $datePrevue
     */
    private $datePrevue;

    /**
     * @var \DateTime|null $dateReelle
     */
    private $dateReelle;

    /**
     * Rencontre constructor
     * 
     * @param string $libelle The "libelle" parameter is a string that represents the label or name of
     * the event or match.
     * @param string $nomEquipeA The parameter "nomEquipeA" represents the name of the team A in a
     * match.
     * @param string $nomEquipeB The parameter "nomEquipeB" represents the name of the second team in a
     * match or game.
     * @param int $scoreEquipeA The parameter "scoreEquipeA" represents the score of team A in a match.
     * @param int $scoreEquipeB The parameter "scoreEquipeB" represents the score of team B in a
     * match.
     * @param string|null $lien The "lien" parameter is a string that represents a link or URL. It is used to
     * store the link related to the event or match.
     * @param \DateTime $datePrevue The parameter "datePrevue" is of type \DateTime and represents the
     * scheduled date for the event.
     * @param \DateTime|null $dateReelle The parameter "dateReelle" is an optional parameter. It
     * represents the actual date and time when the event took place. If the event has not yet taken
     * place, the value of this parameter can be set to null.
     */
    public function __construct(
        string $libelle,
        string $nomEquipeA,
        string $nomEquipeB,
        int $scoreEquipeA,
        int $scoreEquipeB,
        ?string $lien,
        \DateTime $datePrevue,
        ?\DateTime $dateReelle
    )
    {
        $this->libelle = $libelle;
        $this->nomEquipeA = $nomEquipeA;
        $this->nomEquipeB = $nomEquipeB;
        $this->scoreEquipeA = $scoreEquipeA;
        $this->scoreEquipeB = $scoreEquipeB;
        $this->lien = $lien;
        $this->datePrevue = $datePrevue;
        $this->dateReelle = $dateReelle;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getNomEquipeA(): string
    {
        return $this->nomEquipeA;
    }

    /**
     * @return string
     */
    public function getNomEquipeB(): string
    {
        return $this->nomEquipeB;
    }

    /**
     * @return int
     */
    public function getScoreEquipeA(): int
    {
        return $this->scoreEquipeA;
    }

    /**
     * @return int
     */
    public function getScoreEquipeB(): int
    {
        return $this->scoreEquipeB;
    }

    /**
     * @return string|null
     */
    public function getLien(): ?string
    {
        return $this->lien;
    }

    /**
     * @return \DateTime
     */
    public function getDatePrevue(): \DateTime
    {
        return $this->datePrevue;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateReelle(): ?\DateTime
    {
        return $this->dateReelle;
    }
}