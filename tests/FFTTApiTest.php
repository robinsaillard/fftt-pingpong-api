<?php

namespace FFTTApi\Tests;

use FFTTPingpongApi\FFTTApi;
use FFTTPingpongApi\Model\Club;
use PHPUnit\Framework\TestCase;
use FFTTPingpongApi\Model\Equipe;
use FFTTPingpongApi\Model\Joueur;
use FFTTPingpongApi\Model\Partie;
use FFTTPingpongApi\Model\Actualite;
use FFTTPingpongApi\Model\Organisme;
use Symfony\Component\Dotenv\Dotenv;
use FFTTPingpongApi\Model\Classement;
use FFTTPingpongApi\Model\Historique;
use FFTTPingpongApi\Model\ClubDetails;
use FFTTPingpongApi\Model\JoueurDetails;
use FFTTPingpongApi\Exception\JoueurNotFound;
use FFTTPingpongApi\Exception\InvalidCredidentials;
use FFTTPingpongApi\Exception\ClubNotFoundException;
use FFTTPingpongApi\Exception\NoFFTTResponseException;


class FFTTApiTest extends TestCase
{
    public function testInitializeWithGoodParameters()
    {
        $api = $this->generateGoodAPI();
        $this->assertInstanceOf(FFTTApi::class, $api);
        $api->initialize();
    }

    public function testInitializeWithBadParameters()
    {
        $this->expectException(InvalidCredidentials::class);

        $api = new FFTTApi("Bad", "credidentials");
        $api->initialize();
    }

    public function testGetOrganismeZone()
    {
        $api = $this->generateGoodAPI();
        $organismes = $api->getOrganismes("Z");
        $this->assertIsArray($organismes);
        if (!empty($organismes)) {
            $this->assertInstanceOf(Organisme::class, $organismes[0]);
        }
       
    }

    public function testGetOrganismeLigue()
    {
        $api = $this->generateGoodAPI();
        $organismes = $api->getOrganismes("L");
        $this->assertIsArray($organismes);
        if (!empty($organismes)) {
            $this->assertInstanceOf(Organisme::class, $organismes[0]);
        }
    }

    public function testGetOrganismeDepartement()
    {
        $api = $this->generateGoodAPI();
        $organismes = $api->getOrganismes("D");
        $this->assertIsArray($organismes);
        if (!empty($organismes)) {
            $this->assertInstanceOf(Organisme::class, $organismes[0]);
        }
    }


    public function testGetClubsByDepartementWithRealDepartement()
    {
        $api = $this->generateGoodAPI();
        $this->assertIsArray($api->getClubsByDepartement(37));
    }

    public function testGetClubsByDepartementWithBadDepartement()
    {
        $this->expectException(NoFFTTResponseException::class);

        $api = $this->generateGoodAPI();
        $api->getClubsByDepartement(500);
    }

    public function testGetClubDetailsWithGoodNumero()
    {
        $api = $this->generateGoodAPI();
        $club = $api->getClubDetails("04370690");
        $this->assertInstanceOf(ClubDetails::class, $club);
    }

    public function testGetClubDetailsWithBadNumero()
    {
        $this->expectException(ClubNotFoundException::class);

        $api = $this->generateGoodAPI();
        $api->getClubDetails("123");
    }

    public function testGetJoueursByClubWithGoodNumero()
    {
        $api = $this->generateGoodAPI();
        $joueurs = $api->getJoueursByClubFromClassement("04370690");
        if (count($joueurs)) {
            $this->assertInstanceOf(Joueur::class, $joueurs[0]);
        }
    }

    public function testGetJoueursByClubWithBadNumero()
    {
        $this->expectException(ClubNotFoundException::class);

        $api = $this->generateGoodAPI();
        $api->getJoueursByClubFromClassement("500");
    }

    public function testGetJoueursByNomWithGoodNom()
    {
        $api = $this->generateGoodAPI();
        $joueurs = $api->getJoueursByNomFromClassement("Lamirault");
        if (count($joueurs)) {
            $this->assertInstanceOf(Joueur::class, $joueurs[0]);
        }
    }

    public function testGetJoueursByNomWithNomWithSingleQuote()
    {
        $api = $this->generateGoodAPI();
        $joueurs = $api->getJoueursByNomFromClassement("D'ANG");
            $this->assertInstanceOf(Joueur::class, $joueurs[0]);
    }

    public function testGetJoueursByNomWithGoodNomBadPrenom()
    {
        $this->expectException(NoFFTTResponseException::class);

        $api = $this->generateGoodAPI();
        $api->getJoueursByNomFromClassement("Lamirault", "helloooooooooooo");
    }

    public function testGetJoueursByNomWithBadNom()
    {
        $this->expectException(NoFFTTResponseException::class);

        $api = $this->generateGoodAPI();
        $api->getJoueursByNomFromClassement("azertyuiop");
    }

    public function testGetJoueurDetailsByLicenceWithGoodLicence()
    {
        $api = $this->generateGoodAPI();
        $joueur = $api->getJoueurDetailsByLicence(3719655);
        $this->assertInstanceOf(JoueurDetails::class, $joueur);
    }

    public function testGetJoueurDetailsByLicenceWithBadLicence()
    {
        $this->expectException(JoueurNotFound::class);

        $api = $this->generateGoodAPI();
        $api->getJoueurDetailsByLicence(-12);
    }

    public function testGetClassementJoueurByLicenceWithGoodLicence()
    {
        $api = $this->generateGoodAPI();
        $classement = $api->getClassementJoueurByLicence(3719655);
        $this->assertInstanceOf(Classement::class, $classement);
    }

    public function testGetClassementJoueurByLicenceWithBadLicence()
    {
        $this->expectException(JoueurNotFound::class);

        $api = $this->generateGoodAPI();
        $api->getClassementJoueurByLicence(-12);
    }

    public function testGetPartiesJoueurByLicenceWithGoodLicence()
    {
        $api = $this->generateGoodAPI();
        $parties = $api->getPartiesJoueurByLicence(3719655);
        $this->assertIsArray($parties);
        if (count($parties)) {
            $this->assertInstanceOf(Partie::class, $parties[0]);
        }
    }

    public function testGetPartiesJoueurByLicenceWithBadLicence()
    {
        $api = $this->generateGoodAPI();
        $parties = $api->getPartiesJoueurByLicence(-12);
        $this->assertIsArray($parties);
        $this->assertCount(0, $parties);
    }

    public function testGetEquipesByClubWithGoodNumero()
    {
        $api = $this->generateGoodAPI();
        $equipes = $api->getEquipesByClub("04370690", 'M');
        $this->assertIsArray($equipes);
        if (count($equipes)) {
            $this->assertInstanceOf(Equipe::class, $equipes[0]);
        }
    }

    public function testGetEquipesByClubWithBadNumero()
    {
        $this->expectException(NoFFTTResponseException::class);

        $api = $this->generateGoodAPI();
        $api->getEquipesByClub("-12");
    }

    public function testGetHistoriqueJoueurByLicenceWithGoodLicence()
    {
        $api = $this->generateGoodAPI();
        $historiques = $api->getHistoriqueJoueurByLicence(3719655);
        if (count($historiques)) {
            $this->assertInstanceOf(Historique::class, $historiques[0]);
        }
    }

    public function testGetHistoriqueJoueurByLicenceWithBadLicence()
    {
        $this->expectException(JoueurNotFound::class);

        $api = $this->generateGoodAPI();
        $api->getHistoriqueJoueurByLicence(-12);
    }

    public function testGetClubWithName()
    {
        $api = $this->generateGoodAPI();
        $clubs = $api->getClubsByName("seno");
        $this->assertIsArray($clubs);
        if (count($clubs)) {
            $this->assertInstanceOf(Club::class, $clubs[0]);
        }
    }

    public function testGetActualites()
    {
        $api = $this->generateGoodAPI();
        $actualites = $api->getActualites();
        $this->assertIsArray($actualites);
        if (count($actualites)) {
            $this->assertInstanceOf(Actualite::class, $actualites[0]);
        }
    }

    private function generateGoodAPI(): FFTTApi
    {    
        return new FFTTApi(getenv("FFTT_ID"), getenv("FFTT_PASSWORD"));
    }
}