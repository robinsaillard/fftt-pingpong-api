<?php

namespace FFTTPingpongApi\Model;


class Actualite
{

    /**
     * @var \DateTime $date
     */
    public $date;

    /**
     * @var string $titre
     */
    public $titre;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $url
     */
    public $url;

    /**
     * @var string $photo
     */
    public $photo;

    /**
     * Actualite constructor
     * 
     * @param \DateTime $date The `date` parameter represents the date of the event.
     * @param string $titre The "titre" parameter is a string that represents the title of an object or
     * entity.
     * @param string $description The "description" parameter is a string that represents the
     * description of an object. It provides additional information or details about the object.
     * @param string $url The "url" parameter is a string that represents the URL of a webpage or
     * resource. It is used to store the URL associated with the object being constructed.
     * @param string $photo The "photo" parameter is a string that represents the URL or file path of
     * the photo associated with the object being constructed.
     * @param string $categorie The "categorie" parameter is a string that represents the category of
     * the item. It is used to classify or categorize the item in some way.
     */
    public function __construct(\DateTime $date, string $titre, string $description, string $url, string $photo)
    {
        $this->date = $date;
        $this->titre = $titre;
        $this->description = $description;
        $this->url = $url;
        $this->photo = $photo;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

}