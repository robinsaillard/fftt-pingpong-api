<?php

namespace FFTTPingpongApi\Model\Enums;

enum OrganismeType: string implements EnumInterface
{
    case FEDERATION = 'F';
    case ZONE = 'Z';
    case LIGUE = 'L';
    case DEPARTEMENT = 'D';


    /**
     * @return string
     */
    public static function getDefaultType(): string
    {
        return self::ZONE->value;
    }

    /**
     * Check if type exists is enum
     *
     * @param string $type
     * @return bool
     */
    public static function isExisting(string $type): bool
    {
        return AbstractEnum::isExisting(self::cases(), $type);
    }
}
?>
