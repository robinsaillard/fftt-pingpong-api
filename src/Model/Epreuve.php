<?php


namespace FFTTPingpongApi\Model;

class Epreuve
{
    /**
     * @var string $epreuveId
     */
    private $epreuveId;

    /**
     * @var string $organismId
     */
    private $organismId;

    /**
     * @var string $libelle
     */
    private $libelle;

    /**
     * @var string $epreuveType
     */
    private $epreuveType;

    /**
     * Epreuve constructor
     * 
     * @param string $epreuveId A string representing the ID of the epreuve.
     * @param string $organismId The organismId parameter is a string that represents the ID of the
     * organism associated with the epreuve.
     * @param string $libelle The parameter "libelle" is a string that represents the label or name of
     * the object being constructed.
     * @param string $epreuveType The parameter `epreuveType` is likely
     * an enumeration or an object representing the type of the epreuve.
     */
    public function __construct(string $epreuveId, string $organismId, string $libelle, string $epreuveType)
    {
        $this->epreuveId = $epreuveId;
        $this->organismId = $organismId;
        $this->libelle = $libelle;
        $this->epreuveType = $epreuveType;
    }

    /**
     * @return string
     */
    public function getEpreuveId(): string
    {
        return $this->epreuveId;
    }

    /**
     * @return string
     */
    public function getOrganismId(): string
    {
        return $this->organismId;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getEpreuveType(): string
    {
        return $this->epreuveType;
    }
}